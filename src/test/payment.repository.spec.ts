import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import {
  IPaymentDetail,
  IPaymentMaster,
  IPaymentUpdate,
} from '../interface/payment.interface';
import { NotFoundException } from '@nestjs/common';
import { PaymentRepository } from '../repository/payment.repository';
import { Payment } from '../entities/payment.entity';
import { PaymentDetail } from '../entities/payment-detail.entity';
import { EBankChoice } from '../helpers/enum/bank-choice.enum';
import { EPaymentMethod } from '../helpers/enum/payment-method.enum';

describe('PaymentRepository', () => {
  let repository: PaymentRepository;
  let mockPayment: Payment;
  let mockPaymentDetail: PaymentDetail;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PaymentRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<PaymentRepository>(PaymentRepository);

    mockPayment = {
      id: faker.string.uuid(),
      bankChoice: faker.helpers.enumValue(EBankChoice),
      paymentMethod: faker.helpers.enumValue(EPaymentMethod),
      paymentFinal: faker.number.int({ min: 100000 }),
      paymentStatus: faker.helpers.arrayElement(['pending', 'settlement']),
      chargeTime: faker.date.recent().toISOString(),
      checkTime: faker.date.recent().toISOString(),
      paymentTime: faker.date.recent().toISOString(),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
      details: [mockPaymentDetail],
    };

    mockPaymentDetail = {
      id: faker.string.uuid(),
      payment: mockPayment,
      name: faker.string.sample(),
      quantity: faker.number.int({ min: 1 }),
      price: faker.number.int({ min: 100000 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('writeNew', () => {
    it('should return data new payment', async () => {
      // arrange
      const dataPaymentDetail: IPaymentDetail = {
        id: mockPaymentDetail.id,
        name: mockPaymentDetail.name,
        quantity: mockPaymentDetail.quantity,
        price: mockPaymentDetail.price,
      };

      const dataPayment: IPaymentMaster = {
        bankChoice: mockPayment.bankChoice,
        paymentMethod: mockPayment.paymentMethod,
        paymentFinal: mockPayment.paymentFinal,
        paymentStatus: mockPayment.paymentStatus,
        chargeTime: mockPayment.chargeTime,
        details: [dataPaymentDetail],
      };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockPayment);

      // act
      const payment = await repository.writeNew(dataPayment);

      // assert
      expect(payment).toEqual(mockPayment);
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(dataPayment);
    });
  });

  describe('updateStatus', () => {
    it('should return data updated payment', async () => {
      // arrange
      delete mockPayment.details;

      const dataPayment: IPaymentUpdate = {
        id: mockPayment.id,
        paymentStatus: 'settlement',
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
      };

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockPayment);

      mockPayment = { ...mockPayment, ...dataPayment };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockPayment);

      // act
      const payment = await repository.updateStatus(dataPayment);

      // assert
      expect(payment).toEqual(mockPayment);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        where: { id: dataPayment.id },
      });
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(mockPayment);
    });

    it('should throw not found exception', async () => {
      // arrange
      delete mockPayment.details;

      const dataPayment: IPaymentUpdate = {
        id: faker.string.uuid(),
        paymentStatus: 'settlement',
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
      };

      const findOne = jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockPayment);

      // act
      const updateStatus = repository.updateStatus(dataPayment);

      // assert
      await expect(updateStatus).rejects.toEqual(
        new NotFoundException('Payment not found'),
      );
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        where: { id: dataPayment.id },
      });
      expect(saveSpy).toBeCalledTimes(0);
    });
  });
});
