import { Test, TestingModule } from "@nestjs/testing";
import { faker, fakerID_ID } from '@faker-js/faker';
import { HttpException, NotFoundException } from '@nestjs/common';

import { FlightRepository } from "../repository/flight.repository";
import { EAirportCat } from "../helpers/enum/airport-category.enum";
import { EAirportClass } from "../helpers/enum/airport-class.enum";
import { ESeatClass } from "../helpers/enum/seat-class.enum";
import { Airplane } from "../entities/airplane.entity";
import { Flight } from "../entities/flight.entity";
import { ETimezone } from "../helpers/enum/timezone.enum";
import { AirplaneRepository } from "../repository/airplane.repository";
import { AirportRepository } from "../repository/airport.repository";
import { FlightService } from "../services/flight.service";
import { CreateFlightDto } from "../dto/request/create-flight.dto";
import { UpdateFlightDto } from "../dto/request/update-flight.dto";

describe('FlightService', () => {
  let service: FlightService;

  const flightRepo = {
    save: jest.fn(),
    update: jest.fn(),
    findOne: jest.fn(),
    find: jest.fn()
  };

  const airplaneRepo = {
    findOne: jest.fn()
  }

  const airportRepo = {
    findOne: jest.fn()
  }

  const mockBaggage = {
    id: faker.string.uuid(),
    flight: new Flight,
    capacity: faker.number.int({ min: 1 }),
    category: faker.helpers.arrayElement([
      'Carry on Item',
      'Checked Baggage',
      'Cabin Baggage',
    ]),
    chargePrice: faker.number.int({ min: 0 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  const mockPrice = {
    id: faker.string.uuid(),
    seatClass: faker.helpers.enumValue(ESeatClass),
    price: faker.number.int({ min: 500000 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  const mockAirplane: Airplane = {
    id: faker.string.uuid(),
    code: faker.airline.airplane().iataTypeCode,
    registrationNumber: faker.string.alphanumeric(6),
    name: faker.airline.airplane().name,
    maxBusiness: faker.number.int({ min: 0 }),
    maxEconomy: faker.number.int({ min: 50 }),
    totalSeat: faker.number.int({ min: 1 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  const mockAirport = {
    id: faker.string.uuid(),
    icao: faker.airline.airport().iataCode,
    iata: faker.airline.airport().iataCode,
    name: faker.airline.airport().name,
    airportCategory: EAirportCat.DOMESTIK,
    airportClass: EAirportClass.KELAS_III,
    manageBy: "Unit Penyelenggara Bandar Udara",
    address: "Kab. Kutai Barat, Kalimantan Timur",
    city: fakerID_ID.location.city(),
    timezone: ETimezone.WIB,
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  const mockFlight = {
    id: faker.string.uuid(),
    code: faker.airline.flightNumber(),
    airplane: mockAirplane,
    origin: mockAirport,
    destination: mockAirport,
    departureTime: faker.date.anytime().toTimeString(),
    arrivalTime: faker.date.anytime().toTimeString(),
    boardingTime: faker.date.anytime().toTimeString(),
    durationInMinutes: faker.number.int({ min: 60 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
    baggages: [mockBaggage],
    prices: [mockPrice],
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FlightService,
        { provide: FlightRepository, useValue: flightRepo },
        { provide: AirplaneRepository, useValue: airplaneRepo },
        { provide: AirportRepository, useValue: airportRepo },
      ],
    }).compile();
    module.useLogger(false);
    service = module.get<FlightService>(FlightService);
  });

  afterEach(() => jest.clearAllMocks());

  it('should call create method with expected params', async () => {
    const isAirplaneExist = jest
      .spyOn(service, 'isAirplaneExist')
      .mockResolvedValue(mockAirplane);
    jest
      .spyOn(service, 'isAirportExist')
      .mockResolvedValue(mockAirport);
    const createSpy = jest.spyOn(service, 'create');
    const dto: CreateFlightDto = {
      code: mockFlight.code,
      airplane: mockAirplane,
      origin: mockAirport,
      destination: mockAirport,
      departureTime: mockFlight.departureTime,
      arrivalTime: mockFlight.arrivalTime,
      boardingTime: mockFlight.boardingTime,
      durationInMinutes: mockFlight.durationInMinutes
    };
    service.create(dto);
    expect(createSpy).toHaveBeenCalledWith(dto);
    expect(isAirplaneExist).toHaveBeenCalledTimes(1);
    expect(isAirplaneExist).toHaveBeenCalledWith(dto.airplane.id);
  });

  it('should call update method with expected params', async () => {
    const id = mockFlight.id
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockFlight);
    jest
      .spyOn(service, 'isAirplaneExist')
      .mockResolvedValue(mockAirplane);
    jest
      .spyOn(service, 'isAirportExist')
      .mockResolvedValue(mockAirport);
    const updateSpy = jest.spyOn(service, 'update');
    const dto: UpdateFlightDto = {
      code: mockFlight.code,
      airplane: mockAirplane,
      origin: mockAirport,
      destination: mockAirport,
      departureTime: mockFlight.departureTime,
      arrivalTime: mockFlight.arrivalTime,
      boardingTime: mockFlight.boardingTime,
      durationInMinutes: mockFlight.durationInMinutes
    };
    service.update(id, dto);
    expect(updateSpy).toHaveBeenCalledWith(id, dto);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call remove method with expected params', async () => {
    const id = mockFlight.id
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockFlight);
    const removeSpy = jest.spyOn(service, 'remove');
    service.remove(id);
    expect(removeSpy).toHaveBeenCalledWith(id);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call findOne method with expected param', async () => {
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockFlight);
    const findOneSpy = jest.spyOn(service, 'findOne');
    const id = mockFlight.id
    service.findOne(id);
    expect(findOneSpy).toHaveBeenCalledWith(id);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call isExist method with expected param', async () => {
    const findOne = jest
      .spyOn(flightRepo, 'findOne')
      .mockResolvedValue(mockFlight);

    const isExistSpy = jest.spyOn(service, 'isExist');
    const id = mockFlight.id
    service.isExist(id);
    expect(isExistSpy).toHaveBeenCalledWith(id);
    expect(findOne).toHaveBeenCalledTimes(1);
  });

  it('should call isExist method with expected param and throw not found exception', async () => {
    const findOne = jest
      .spyOn(flightRepo, 'findOne')
      .mockResolvedValue(null);

    const id = mockBaggage.id
    const isExist = service.isExist(id);
    await expect(isExist).rejects.toEqual(
      new NotFoundException('Flight not found'),
    );
    expect(findOne).toHaveBeenCalledTimes(1);
  });

  it('should call isAirplaneExist method with expected param', async () => {
    const findOne = jest
      .spyOn(airplaneRepo, 'findOne')
      .mockResolvedValue(mockAirplane);

    const isExistSpy = jest.spyOn(service, 'isAirplaneExist');
    const id = mockAirplane.id
    service.isAirplaneExist(id);
    expect(isExistSpy).toHaveBeenCalledWith(id);
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({ "where": { "id": id } });
  });

  it('should call isAirplaneExist method with expected param and throw not found exception', async () => {
    const findOne = jest
      .spyOn(airplaneRepo, 'findOne')
      .mockResolvedValue(null);

    const id = mockAirplane.id
    const isExist = service.isAirplaneExist(id);
    await expect(isExist).rejects.toEqual(
      new NotFoundException('Airplane not found'),
    );
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({ "where": { "id": id } });
  });

  it('should call isAirportExist method with expected param', async () => {
    const findOne = jest
      .spyOn(airportRepo, 'findOne')
      .mockResolvedValue(mockAirport);

    const isExistSpy = jest.spyOn(service, 'isAirportExist');
    const id = mockAirport.id
    service.isAirportExist(id);
    expect(isExistSpy).toHaveBeenCalledWith(id);
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({ "where": { "id": id } });
  });

  it('should call isAirportExist method with expected param and throw not found exception', async () => {
    const findOne = jest
      .spyOn(airportRepo, 'findOne')
      .mockResolvedValue(null);

    const id = mockAirport.id
    const isExist = service.isAirportExist(id);
    await expect(isExist).rejects.toEqual(
      new NotFoundException('Airport not found'),
    );
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({ "where": { "id": id } });
  });

  it('should call findAll method with expected param', async () => {
    jest
      .spyOn(flightRepo, 'find')
      .mockResolvedValue([mockFlight]);
    const findAllSpy = jest.spyOn(service, 'findAll')
    const result = await service.findAll();
    expect(findAllSpy).toHaveBeenCalled();
    expect(result).toEqual([mockFlight]);
  });

  it('should call findAll method with expected param and throw not found exception', async () => {
    const find = jest
      .spyOn(flightRepo, 'find')
      .mockResolvedValue(null);
    const result = service.findAll();
    await expect(result).rejects.toEqual(
      new NotFoundException('flight data not found'),
    );
    expect(find).toHaveBeenCalledTimes(1);
  });
})
