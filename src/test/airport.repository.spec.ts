import { Test, TestingModule } from '@nestjs/testing';
import { DataSource, Like } from 'typeorm';
import { faker, fakerID_ID } from '@faker-js/faker';
import { AirportRepository } from '../repository/airport.repository';
import { Airport } from '../entities/airport.entity';
import { EAirportCat } from '../helpers/enum/airport-category.enum';
import { EAirportClass } from '../helpers/enum/airport-class.enum';
import { ETimezone } from '../helpers/enum/timezone.enum';
import { FilterAirportDto } from '../dto/core/filter-airport.dto';

describe('AirportRepository', () => {
  let repository: AirportRepository;
  let mockAirport: Airport;
  let mockQueryName: string;
  let mockQueryCity: string;
  let mockQueryCodeIcao: string;
  let mockQueryCodeIata: string;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AirportRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<AirportRepository>(AirportRepository);

    mockAirport = {
      id: faker.string.uuid(),
      icao: faker.airline.airport().iataCode,
      iata: faker.airline.airport().iataCode,
      name: faker.airline.airport().name,
      airportCategory: EAirportCat.DOMESTIK,
      airportClass: EAirportClass.KELAS_III,
      manageBy: "Unit Penyelenggara Bandar Udara",
      address: "Kab. Kutai Barat, Kalimantan Timur",
      city: fakerID_ID.location.city(),
      timezone: ETimezone.WIB,
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };

    mockQueryName =
      mockAirport.name.length <= 5
        ? mockAirport.name
        : mockAirport.name.substring(
            2,
            Math.floor(Math.random() * (mockAirport.name.length - 3) + 3),
          );

    mockQueryCity =
      mockAirport.city.length <= 5
        ? mockAirport.city
        : mockAirport.city.substring(
            2,
            Math.floor(Math.random() * (mockAirport.city.length - 3) + 3),
          );

    mockQueryCodeIcao = mockAirport.icao.substring(1);
    mockQueryCodeIata = mockAirport.iata.substring(1);
  });

  afterEach(() => jest.clearAllMocks());

  describe('findWithQuery', () => {
    it('should return list airport without criteria', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: null,
        icao: null,
        iata: null,
        city: null,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toHaveBeenCalledWith({ where: {} });
    });

    it('should return list airport with name contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: mockQueryName,
        icao: null,
        iata: null,
        city: null,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: { name: Like('%' + mockQueryName + '%') },
      });
      airports.forEach((airport) => {
        expect(airport.name).toContain(mockQueryName);
      });
    });

    it('should return list airport with icao contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: null,
        icao: mockQueryCodeIcao,
        iata: null,
        city: null,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toHaveBeenCalledWith({
        where: { icao: Like('%' + mockQueryCodeIcao + '%') },
      });
      airports.forEach((airport) => {
        expect(airport.icao).toContain(mockQueryCodeIcao);
      });
    });

    it('should return list airport with iata contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: null,
        icao: null,
        iata: mockQueryCodeIata,
        city: null,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toHaveBeenCalledWith({
        where: { iata: Like('%' + mockQueryCodeIata + '%') },
      });
      airports.forEach((airport) => {
        expect(airport.iata).toContain(mockQueryCodeIata);
      });
    });

    it('should return list airport with city contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: null,
        city: mockQueryCity,
        icao: null,
        iata: null,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: { city: Like('%' + mockQueryCity + '%') },
      });
      airports.forEach((airport) => {
        expect(airport.city).toContain(mockQueryCity);
      });
    });

    it('should return list airport with name, code and city contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: mockQueryName,
        city: mockQueryCity,
        icao: mockQueryCodeIcao,
        iata: mockQueryCodeIata,
      };

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: {
          name: Like('%' + mockQueryName + '%'),
          city: Like('%' + mockQueryCity + '%'),
          icao: Like('%' + mockQueryCodeIcao + '%'),
          iata: Like('%' + mockQueryCodeIata + '%'),
        },
      });
      airports.forEach((airport) => {
        expect(airport.name).toContain(mockQueryName);
      });
      airports.forEach((airport) => {
        expect(airport.city).toContain(mockQueryCity);
      });
      airports.forEach((airport) => {
        expect(airport.icao).toContain(mockQueryCodeIcao);
      });
      airports.forEach((airport) => {
        expect(airport.iata).toContain(mockQueryCodeIata);
      });
    });

    it('should return empty array', async () => {
      // arrange
      const differentName = faker.airline.airport().name;
      const differentCity = fakerID_ID.location.city();

      const name =
        differentName.length <= 5
          ? differentName
          : differentName.substring(
              2,
              Math.floor(Math.random() * (differentName.length - 3) + 3),
            );

      const city =
        differentCity.length <= 5
          ? differentCity
          : differentCity.substring(
              2,
              Math.floor(Math.random() * (differentCity.length - 3) + 3),
            );

      const query: FilterAirportDto = {
        city,
        name,
        icao: null,
        iata: null,
      };

      const findSpy = jest.spyOn(repository, 'find').mockResolvedValue([]);

      // act
      const airports = await repository.findWithQuery(query);

      // assert
      expect(airports).toEqual([]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: { city: Like('%' + city + '%'), name: Like('%' + name + '%') },
      });
    });
  });
});
