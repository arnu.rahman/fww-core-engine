import { Test, TestingModule } from "@nestjs/testing";
import { faker } from "@faker-js/faker";
import { ForbiddenException } from '@nestjs/common';
import { ReservationService } from "../services/reservation.service";
import { ReservationController } from "../controllers/reservation.controller";
import { ReservationDto } from "../dto/core/reservation.dto";
import { IReservationUpdateRequest } from "../interface/reservation-update.interface";
import { EReservationStatus } from "../helpers/enum/reservation-status.enum";
import { Payment } from "../entities/payment.entity";
import { PaymentDetail } from "../entities/payment-detail.entity";
import { EBankChoice } from "../helpers/enum/bank-choice.enum";
import { EPaymentMethod } from "../helpers/enum/payment-method.enum";
import { IPaymentDetail, IPaymentMaster, IPaymentUpdate } from "../interface/payment.interface";

describe("ReservationController", () => {
  let controller: ReservationController
  let spyService: ReservationService

  beforeAll(async () => {
    const ApiServiceProvider = {
      provide: ReservationService,
      useFactory: () => ({
        createReservation: jest.fn(() => []),
        updateReservation: jest.fn(() => []),
        getReservation: jest.fn(() => { }),
        getReservationByCode: jest.fn(() => { }),
        chargePayment: jest.fn(() => { }),
        updatePayment: jest.fn(() => { }),
      })
    }
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ReservationController],
      providers: [ReservationService, ApiServiceProvider],
    }).compile();

    controller = app.get<ReservationController>(ReservationController);
    spyService = app.get<ReservationService>(ReservationService);
  })


  describe('handleChargePayment', () => {
    let mockPayment: Payment;
    let mockPaymentDetail: PaymentDetail;

    beforeEach(async () => {
      mockPayment = {
        id: faker.string.uuid(),
        bankChoice: faker.helpers.enumValue(EBankChoice),
        paymentMethod: faker.helpers.enumValue(EPaymentMethod),
        paymentFinal: faker.number.int({ min: 100000 }),
        paymentStatus: faker.helpers.arrayElement(['pending', 'settlement']),
        chargeTime: faker.date.recent().toISOString(),
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        details: [mockPaymentDetail],
      };

      mockPaymentDetail = {
        id: faker.string.uuid(),
        payment: mockPayment,
        name: faker.string.sample(),
        quantity: faker.number.int({ min: 1 }),
        price: faker.number.int({ min: 100000 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it("calling handleChargePayment method", () => {
      const dataPaymentDetail: IPaymentDetail = {
        id: mockPaymentDetail.id,
        name: mockPaymentDetail.name,
        quantity: mockPaymentDetail.quantity,
        price: mockPaymentDetail.price,
      };

      const dto: IPaymentMaster = {
        bankChoice: mockPayment.bankChoice,
        paymentMethod: mockPayment.paymentMethod,
        paymentFinal: mockPayment.paymentFinal,
        paymentStatus: mockPayment.paymentStatus,
        chargeTime: mockPayment.chargeTime,
        details: [dataPaymentDetail],
      };
      expect(controller.handleChargePayment(dto)).not.toEqual(null);
    })
  })

  describe('handleUpdateReservationSystemTask', () => {
    it("calling handleUpdateReservationSystemTask method", () => {
      const dto: IReservationUpdateRequest = {
        id: faker.string.uuid(),
        status: faker.helpers.enumValue(EReservationStatus),
        journeyTime: faker.date.recent().toISOString(),
        bookingCode: faker.string.alphanumeric(4)
      };
      expect(controller.handleUpdateReservationSystemTask(dto)).not.toEqual(null);
    })
  })

  describe('newReservation', () => {
    it("calling newReservation method", () => {
      const dto = new ReservationDto;
      expect(controller.newReservation(dto)).not.toEqual(null);
    })

    it('should throw forbidden exception when newReservation', async () => {
      // arrange
      const dto = new ReservationDto
      const serviceSpy = jest
        .spyOn(spyService, 'createReservation')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.newReservation(dto);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(dto);
    });
  })

  describe('updateReservation', () => {
    it("calling updateReservation method", () => {
      const dto: IReservationUpdateRequest = {
        id: faker.string.uuid(),
        status: faker.helpers.enumValue(EReservationStatus),
        journeyTime: faker.date.recent().toISOString(),
        bookingCode: faker.string.alphanumeric(4)
      };
      expect(controller.updateReservation(dto)).not.toEqual(null);
    })

    it('should throw forbidden exception when updateReservation and handleUpdateReservationSystemTask', async () => {
      // arrange
      const dto: IReservationUpdateRequest = {
        id: faker.string.uuid(),
        status: faker.helpers.enumValue(EReservationStatus),
        journeyTime: faker.date.recent().toISOString(),
        bookingCode: faker.string.alphanumeric(4)
      }; const serviceSpy = jest
        .spyOn(spyService, 'updateReservation')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.updateReservation(dto);
      const controllerSpyHandler = controller.handleUpdateReservationSystemTask(dto);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      await expect(controllerSpyHandler).rejects.toEqual(
        new ForbiddenException('Error in handleUpdateReservation'),
      );
      expect(serviceSpy).toHaveBeenCalledWith(dto);
    });
  })

  describe('getReservation', () => {
    it("calling getReservation method", () => {
      const id = faker.string.uuid()
      expect(controller.getReservation(id)).not.toEqual(null);
    })

    it('should throw forbidden exception when getReservation', async () => {
      // arrange
      const id = faker.string.uuid()
      const serviceSpy = jest
        .spyOn(spyService, 'getReservation')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.getReservation(id);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(id);
    });
  })

  describe('getByReservationCode', () => {
    it("calling getByReservationCode method", () => {
      const identityNumber = faker.string.numeric({ length: 16 })
      const reservationCode = faker.string.alphanumeric(6)
      expect(controller.getByReservationCode(identityNumber, reservationCode)).not.toEqual(null);
    })

    it('should throw forbidden exception when getByReservationCode', async () => {
      // arrange
      const identityNumber = faker.string.numeric({ length: 16 })
      const reservationCode = faker.string.alphanumeric(6)
      const serviceSpy = jest
        .spyOn(spyService, 'getReservationByCode')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.getByReservationCode(identityNumber, reservationCode);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(identityNumber, reservationCode);
    });
  })

  describe('chargePayment', () => {
    let mockPayment: Payment;
    let mockPaymentDetail: PaymentDetail;

    beforeEach(async () => {
      mockPayment = {
        id: faker.string.uuid(),
        bankChoice: faker.helpers.enumValue(EBankChoice),
        paymentMethod: faker.helpers.enumValue(EPaymentMethod),
        paymentFinal: faker.number.int({ min: 100000 }),
        paymentStatus: faker.helpers.arrayElement(['pending', 'settlement']),
        chargeTime: faker.date.recent().toISOString(),
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        details: [mockPaymentDetail],
      };

      mockPaymentDetail = {
        id: faker.string.uuid(),
        payment: mockPayment,
        name: faker.string.sample(),
        quantity: faker.number.int({ min: 1 }),
        price: faker.number.int({ min: 100000 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it("calling chargePayment method", () => {
      const dataPaymentDetail: IPaymentDetail = {
        id: mockPaymentDetail.id,
        name: mockPaymentDetail.name,
        quantity: mockPaymentDetail.quantity,
        price: mockPaymentDetail.price,
      };

      const dto: IPaymentMaster = {
        bankChoice: mockPayment.bankChoice,
        paymentMethod: mockPayment.paymentMethod,
        paymentFinal: mockPayment.paymentFinal,
        paymentStatus: mockPayment.paymentStatus,
        chargeTime: mockPayment.chargeTime,
        details: [dataPaymentDetail],
      };
      expect(controller.chargePayment(dto)).not.toEqual(null);
    })

    it('should throw forbidden exception when chargePayment and handleChargePayment', async () => {
      // arrange
      const dataPaymentDetail: IPaymentDetail = {
        id: mockPaymentDetail.id,
        name: mockPaymentDetail.name,
        quantity: mockPaymentDetail.quantity,
        price: mockPaymentDetail.price,
      };

      const dto: IPaymentMaster = {
        bankChoice: mockPayment.bankChoice,
        paymentMethod: mockPayment.paymentMethod,
        paymentFinal: mockPayment.paymentFinal,
        paymentStatus: mockPayment.paymentStatus,
        chargeTime: mockPayment.chargeTime,
        details: [dataPaymentDetail],
      };
      const serviceSpy = jest
        .spyOn(spyService, 'chargePayment')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.chargePayment(dto);
      const controllerSpyHandler = controller.handleChargePayment(dto);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      await expect(controllerSpyHandler).rejects.toEqual(
        new ForbiddenException('Error in handleChargePayment'),
      );
      expect(serviceSpy).toHaveBeenCalledWith(dto);
    });
  })

  describe('handleUpdatePayment', () => {
    let mockPayment: Payment;
    let mockPaymentDetail: PaymentDetail;

    beforeEach(async () => {
      mockPayment = {
        id: faker.string.uuid(),
        bankChoice: faker.helpers.enumValue(EBankChoice),
        paymentMethod: faker.helpers.enumValue(EPaymentMethod),
        paymentFinal: faker.number.int({ min: 100000 }),
        paymentStatus: faker.helpers.arrayElement(['pending', 'settlement']),
        chargeTime: faker.date.recent().toISOString(),
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        details: [mockPaymentDetail],
      };

      mockPaymentDetail = {
        id: faker.string.uuid(),
        payment: mockPayment,
        name: faker.string.sample(),
        quantity: faker.number.int({ min: 1 }),
        price: faker.number.int({ min: 100000 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it("calling handleUpdatePayment method", () => {
      const dto: IPaymentUpdate = {
        id: faker.string.uuid(),
        paymentStatus: "PAID",
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
      }
      expect(controller.handleUpdatePayment(dto)).not.toEqual(null);
    })

    it('should throw forbidden exception when handleUpdatePayment', async () => {
      // arrange
      const dto: IPaymentUpdate = {
        id: faker.string.uuid(),
        paymentStatus: "PAID",
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.recent().toISOString(),
      }
      const serviceSpy = jest
        .spyOn(spyService, 'updatePayment')
        .mockRejectedValue(
          new ForbiddenException('Error in handleUpdatePayment'),
        );

      // act
      const controllerSpy = controller.handleUpdatePayment(dto);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Error in handleUpdatePayment'),
      );
      expect(serviceSpy).toHaveBeenCalledWith(dto);
    });
  })
})
