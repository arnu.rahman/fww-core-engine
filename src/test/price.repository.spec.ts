import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { PriceRepository } from '../repository/price.repository';
import { Price } from '../entities/price.entity';
import { ESeatClass } from '../helpers/enum/seat-class.enum';
import { IPriceSearch } from '../interface/price-search.interface';

describe('PriceRepository', () => {
  let repository: PriceRepository;
  let mockPrice: Price;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PriceRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<PriceRepository>(PriceRepository);

    mockPrice = {
      id: faker.string.uuid(),
      seatClass: faker.helpers.enumValue(ESeatClass),
      price: faker.number.int({ min: 500000 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });

  it('should return price data from method findByFlightAndSeatClass', async () => {
    // arrange
    const createQueryBuilder: any = {
      where: () => createQueryBuilder,
      andWhere: () => createQueryBuilder,
      getOne: jest.fn().mockReturnValue(mockPrice)
    }
    jest.spyOn(repository, 'createQueryBuilder').mockImplementation(() => createQueryBuilder)
    const dataPrice: IPriceSearch = {
      flight: faker.string.uuid(),
      seatClass: faker.helpers.enumValue(ESeatClass),
    };

    // act
    const getData = await repository.findByFlightAndSeatClass(dataPrice);

    // assert
    expect(getData).toEqual(mockPrice);
  });
});
