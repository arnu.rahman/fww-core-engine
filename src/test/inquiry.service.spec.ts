import { Test, TestingModule } from '@nestjs/testing';
import { faker, fakerID_ID } from '@faker-js/faker';

import { AirportRepository } from '../repository/airport.repository';
import { FlightRepository } from '../repository/flight.repository';
import { PriceRepository } from '../repository/price.repository';
import { SeatRepository } from '../repository/seat.repository';
import { Airport } from '../entities/airport.entity';
import {
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Airplane } from '../entities/airplane.entity';
import { Flight } from '../entities/flight.entity';
import { Price } from '../entities/price.entity';
import { ISeatValidate } from '../interface/seat-validate.interface';
import { Seat } from '../entities/seat.entity';
import { IPriceSearch } from '../interface/price-search.interface';
import { ISeat } from '../interface/seat.interface';
import { IAirplaneWithSpecificSeat } from '../interface/airplane-with-specific-seat.interface';
import { IFlightWithSpecificSeat } from '../interface/flight-with-specific-seat.interface';
import { Baggage } from '../entities/baggage.entity';
import { InquiryService } from '../services/inquiry.service';
import { EAirportCat } from '../helpers/enum/airport-category.enum';
import { EAirportClass } from '../helpers/enum/airport-class.enum';
import { ETimezone } from '../helpers/enum/timezone.enum';
import { FilterAirportDto } from '../dto/core/filter-airport.dto';
import { FilterFlightDto } from '../dto/core/filter-flight.dto';
import { FlightCandidateDto } from '../dto/core/flight-candidate.dto';
import { ESeatClass } from '../helpers/enum/seat-class.enum';
import { FlightSeatDto } from '../dto/core/flight-seat.dto';
import { ESeatSide } from '../helpers/enum/seat-side.enum';
import { ESeatPosition } from '../helpers/enum/seat-position.enum';
import { SeatClassDto } from '../dto/core/seat-class.dto';

describe('InquiryService', () => {
  let service: InquiryService;

  const airportRepo = {
    findWithQuery: jest.fn(),
  };

  const flightRepo = {
    findCandidate: jest.fn(),
    findWithQuery: jest.fn(),
    findByIdIncludeSeats: jest.fn(),
    findByIdIncludeSpecificSeat: jest.fn(),
    findByIdIncludeBaggages: jest.fn(),
    findByIdIncludeAirports: jest.fn(),
  };

  const priceRepo = {
    findByFlightAndSeatClass: jest.fn(),
  };

  const seatRepo = {
    findOneBySeatId: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        InquiryService,
        { provide: AirportRepository, useValue: airportRepo },
        { provide: FlightRepository, useValue: flightRepo },
        { provide: PriceRepository, useValue: priceRepo },
        { provide: SeatRepository, useValue: seatRepo },
      ],
    }).compile();
    module.useLogger(false);
    service = module.get<InquiryService>(InquiryService);
  });

  afterEach(() => jest.clearAllMocks());

  describe('searchAirport', () => {
    let mockAirport: Airport;
    let mockQueryName: string;
    let mockQueryCity: string;
    let mockQueryCodeIcao: string;
    let mockQueryCodeIata: string;

    beforeEach(async () => {
      mockAirport = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockQueryName =
        mockAirport.name.length <= 5
          ? mockAirport.name
          : mockAirport.name.substring(
            2,
            Math.floor(Math.random() * (mockAirport.name.length - 3) + 3),
          );

      mockQueryCity =
        mockAirport.city.length <= 5
          ? mockAirport.city
          : mockAirport.city.substring(
            2,
            Math.floor(Math.random() * (mockAirport.city.length - 3) + 3),
          );

      mockQueryCodeIcao = mockAirport.icao.substring(1);
      mockQueryCodeIata = mockAirport.iata.substring(1);
    });

    afterEach(() => jest.clearAllMocks());

    it('should return list airport without criteria', async () => {
      // arrange
      const query: FilterAirportDto = null;
      const findWithQuery = jest
        .spyOn(airportRepo, 'findWithQuery')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await service.searchAirport(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findWithQuery).toHaveBeenCalledTimes(1);
      expect(findWithQuery).toHaveBeenCalledWith(null);
    });

    it('should return list airport with name, city and code contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        city: mockQueryCity,
        name: mockQueryName,
        icao: mockQueryCodeIcao,
        iata: mockQueryCodeIata,
      };
      const findWithQuery = jest
        .spyOn(airportRepo, 'findWithQuery')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await service.searchAirport(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(findWithQuery).toHaveBeenCalledTimes(1);
      expect(findWithQuery).toHaveBeenCalledWith(query);
      airports.forEach((airport) => expect(airport.name).toContain(query.name));
      airports.forEach((airport) => expect(airport.city).toContain(query.city));
      airports.forEach((airport) => expect(airport.icao).toContain(query.icao));
      airports.forEach((airport) => expect(airport.iata).toContain(query.iata));
    });

    it('should throw not found exception', async () => {
      // arrange
      const differentName = faker.airline.airport().name;
      const differentCity = fakerID_ID.location.city();

      const name =
        differentName.length <= 5
          ? differentName
          : differentName.substring(
            2,
            Math.floor(Math.random() * (differentName.length - 3) + 3),
          );

      const city =
        differentCity.length <= 5
          ? differentCity
          : differentCity.substring(
            2,
            Math.floor(Math.random() * (differentCity.length - 3) + 3),
          );

      const query: FilterAirportDto = {
        name,
        icao: null,
        iata: null,
        city,
      };

      const findWithQuery = jest
        .spyOn(airportRepo, 'findWithQuery')
        .mockResolvedValue([]);

      // act
      const searchAirport = service.searchAirport(query);

      // assert
      await expect(searchAirport).rejects.toEqual(
        new NotFoundException('Airport not found'),
      );
      expect(findWithQuery).toHaveBeenCalledTimes(1);
      expect(findWithQuery).toHaveBeenCalledWith(query);
    });
  });

  describe('filterFlight', () => {
    let mockDeparture: Airport;
    let mockDestination: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;

    let mockQueryDeparture: string;
    let mockQueryDestination: string;

    beforeEach(async () => {
      mockDeparture = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockDestination = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      }

      mockAirplane = {
        id: faker.string.uuid(),
        code: faker.airline.airplane().iataTypeCode,
        registrationNumber: faker.string.alphanumeric(6),
        name: faker.airline.airplane().name,
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        totalSeat: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.string.uuid(),
        code: faker.airline.flightNumber(),
        airplane: mockAirplane,
        origin: mockDeparture,
        destination: mockDestination,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        boardingTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockQueryDeparture =
        mockDeparture.name.length <= 5
          ? mockDeparture.name
          : mockDeparture.name.substring(
            2,
            Math.floor(Math.random() * (mockDeparture.name.length - 3) + 3),
          );

      mockQueryDestination =
        mockDestination.city.length <= 5
          ? mockDestination.city
          : mockDestination.city.substring(
            2,
            Math.floor(Math.random() * (mockDestination.city.length - 3) + 3),
          );
    });

    afterEach(() => jest.clearAllMocks());

    it('should return list flight without criteria', async () => {
      // arrange
      const query: FilterFlightDto = null;
      const findWithQuery = jest
        .spyOn(flightRepo, 'findWithQuery')
        .mockResolvedValue([mockFlight]);

      // act
      const flights = await service.filterFlight(query);

      // assert
      expect(flights).toEqual([mockFlight]);
      expect(findWithQuery).toHaveBeenCalledTimes(1);
      expect(findWithQuery).toHaveBeenCalledWith(query);
    });

    it('should return list flight with departure and destination contains criteria value', async () => {
      // arrange
      const query: FilterFlightDto = {
        origin: mockQueryDeparture,
        destination: mockQueryDestination,
      };

      const findWithQuery = jest
        .spyOn(flightRepo, 'findWithQuery')
        .mockResolvedValue([mockFlight]);

      // act
      const flights = await service.filterFlight(query);

      // assert
      expect(flights).toEqual([mockFlight]);
      expect(findWithQuery).toHaveBeenCalledTimes(1);
      expect(findWithQuery).toHaveBeenCalledWith(query);
      flights.forEach((flight) =>
        expect(
          flight.origin.name.includes(query.origin) ||
          flight.origin.city.includes(query.origin) ||
          flight.origin.icao.includes(query.origin) ||
          flight.origin.iata.includes(query.origin),
        ).toBe(true),
      );

      flights.forEach((flight) =>
        expect(
          flight.destination.name.includes(query.destination) ||
          flight.destination.city.includes(query.destination) ||
          flight.destination.icao.includes(query.destination) ||
          flight.destination.iata.includes(query.destination),
        ).toBe(true),
      );
    });

    it('should throw not found exception', async () => {
      // arrange
      const origin = faker.string.sample();
      const destination = faker.string.sample();

      const query: FilterFlightDto = {
        origin,
        destination,
      };

      const findWithQuery = jest
        .spyOn(flightRepo, 'findWithQuery')
        .mockResolvedValue([]);

      // act
      const filterFlight = service.filterFlight(query);

      // assert
      await expect(filterFlight).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(findWithQuery).toHaveBeenCalledTimes(1);
      expect(findWithQuery).toHaveBeenCalledWith(query);
    });
  });

  describe('searchFlight', () => {
    let mockDeparture: Airport;
    let mockDestination: Airport;
    let mockAirplane: Airplane;
    let mockPrice: Price;
    let mockFlight: Flight;

    let mockFlightDto: FlightCandidateDto;

    beforeEach(async () => {
      mockDeparture = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockDestination = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.string.uuid(),
        code: faker.airline.airplane().iataTypeCode,
        registrationNumber: faker.string.alphanumeric(6),
        name: faker.airline.airplane().name,
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        totalSeat: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPrice = {
        id: faker.string.uuid(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        price: faker.number.int({ min: 500000 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.string.uuid(),
        code: faker.airline.flightNumber(),
        airplane: mockAirplane,
        origin: mockDeparture,
        destination: mockDestination,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        boardingTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        prices: [mockPrice],
      };

      mockFlightDto = {
        originAirportId: mockDeparture.id,
        destinationAirportId: mockDestination.id,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return list candidate flight', async () => {
      // arrange
      const findCandidate = jest
        .spyOn(flightRepo, 'findCandidate')
        .mockResolvedValue([mockFlight]);

      // act
      const flights = await service.searchFlight(mockFlightDto);

      // assert
      expect(flights).toEqual([mockFlight]);
      expect(findCandidate).toHaveBeenCalledTimes(1);
      expect(findCandidate).toHaveBeenCalledWith(mockFlightDto);
      flights.forEach((flight) => {
        expect(flight.origin.id).toEqual(mockFlightDto.originAirportId);
        expect(flight.destination.id).toEqual(mockFlightDto.destinationAirportId);
      });
    });

    it('should throw not found exception', async () => {
      // arrange
      mockFlightDto = {
        originAirportId: faker.string.uuid(),
        destinationAirportId: faker.string.uuid(),
      };

      const findCandidate = jest
        .spyOn(flightRepo, 'findCandidate')
        .mockResolvedValue([]);

      // act
      const filterFlight = service.searchFlight(mockFlightDto);

      // assert
      await expect(filterFlight).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(findCandidate).toHaveBeenCalledTimes(1);
      expect(findCandidate).toHaveBeenCalledWith(mockFlightDto);
    });
  });

  describe('seatValidate', () => {
    let mockSeat: Seat;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockDeparture: Airport;
    let mockDestination: Airport;
    let mockPrice: Price;
    let mockSeatValidate: ISeatValidate;
    let mockFlightSeatDto: FlightSeatDto;

    beforeEach(async () => {
      mockSeat = {
        id: faker.string.uuid(),
        code: faker.string.alphanumeric(2),
        seatClass: faker.helpers.enumValue(ESeatClass),
        seatSide: faker.helpers.enumValue(ESeatSide),
        seatPosition: faker.helpers.enumValue(ESeatPosition),
        airplane: new Airplane,
        seqRow: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.string.uuid(),
        code: faker.airline.airplane().iataTypeCode,
        registrationNumber: faker.string.alphanumeric(6),
        name: faker.airline.airplane().name,
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        totalSeat: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        seats: [mockSeat],
      };

      mockDeparture = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockDestination = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.string.uuid(),
        code: faker.airline.flightNumber(),
        airplane: mockAirplane,
        origin: mockDeparture,
        destination: mockDestination,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        boardingTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPrice = {
        id: faker.string.uuid(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        price: faker.number.int({ min: 500000 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlightSeatDto = {
        flight: mockFlight.id,
        seat: mockSeat.id,
      };

      mockSeatValidate = {
        isValid: true,
        flight: mockFlightSeatDto.flight,
        origin: mockDeparture,
        destination: mockDestination,
        airplane: mockFlight.airplane.id,
        seat: mockFlightSeatDto.seat,
        seatClass: mockSeat.seatClass,
        seatNumber: mockSeat.code,
        price: mockPrice.price,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return success validation result', async () => {
      // arrange
      const findOneBySeatId = jest
        .spyOn(seatRepo, 'findOneBySeatId')
        .mockResolvedValue(mockSeat);

      const findByIdIncludeSpecificSeat = jest
        .spyOn(flightRepo, 'findByIdIncludeSpecificSeat')
        .mockResolvedValue(mockFlight);

      const findByFlightAndSeatClass = jest
        .spyOn(priceRepo, 'findByFlightAndSeatClass')
        .mockResolvedValue(mockPrice);

      const priceCriteria: IPriceSearch = {
        flight: mockFlightSeatDto.flight,
        seatClass: mockSeat.seatClass,
      };

      const specificSeat: ISeat = {
        id: mockSeat.id,
        code: mockSeat.code,
        seatClass: mockSeat.seatClass,
        side: mockSeat.seatSide,
        position: mockSeat.seatPosition,
      };

      const airplaneWithSpecificSeat: IAirplaneWithSpecificSeat = {
        id: mockAirplane.id,
        name: mockAirplane.name,
        registrationNumber: mockAirplane.registrationNumber,
        maxBusiness: mockAirplane.maxBusiness,
        maxEconomy: mockAirplane.maxEconomy,
        totalSeat: mockAirplane.totalSeat,
        seat: specificSeat,
      };

      const flightWithSpecificSeat: IFlightWithSpecificSeat = {
        id: mockFlight.id,
        code: mockFlight.code,
        origin: mockFlight.origin,
        destination: mockFlight.destination,
        airplane: airplaneWithSpecificSeat,
        departureTimeInWIB: mockFlight.departureTime,
        arrivalTimeInWIB: mockFlight.arrivalTime,
        durationInMinutes: mockFlight.durationInMinutes,
      };

      mockSeatValidate = {
        isValid: true,
        flight: mockFlightSeatDto.flight,
        origin: flightWithSpecificSeat.origin,
        destination: flightWithSpecificSeat.destination,
        airplane: flightWithSpecificSeat.airplane.id,
        seat: mockFlightSeatDto.seat,
        seatClass: mockSeat.seatClass,
        seatNumber: mockSeat.code,
        price: mockPrice.price,
      };

      // act
      const seatValidate = await service.seatValidate(mockFlightSeatDto);

      // assert
      expect(seatValidate).toEqual(mockSeatValidate);
      expect(findOneBySeatId).toHaveBeenCalledTimes(1);
      expect(findOneBySeatId).toHaveBeenCalledWith(mockFlightSeatDto.seat);
      expect(findByIdIncludeSpecificSeat).toHaveBeenCalledTimes(1);
      expect(findByIdIncludeSpecificSeat).toHaveBeenCalledWith(
        mockFlightSeatDto,
      );
      expect(findByFlightAndSeatClass).toHaveBeenCalledTimes(1);
      expect(findByFlightAndSeatClass).toHaveBeenCalledWith(priceCriteria);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const findOneBySeatId = jest
        .spyOn(seatRepo, 'findOneBySeatId')
        .mockResolvedValue(mockSeat);

      const findByIdIncludeSpecificSeat = jest
        .spyOn(flightRepo, 'findByIdIncludeSpecificSeat')
        .mockResolvedValue(null);

      const findByFlightAndSeatClass = jest
        .spyOn(priceRepo, 'findByFlightAndSeatClass')
        .mockResolvedValue(mockPrice);

      // act
      const seatValidate = service.seatValidate(mockFlightSeatDto);

      // assert
      await expect(seatValidate).rejects.toEqual(
        new UnprocessableEntityException('Invalid input seat info'),
      );
      expect(findOneBySeatId).toHaveBeenCalledTimes(1);
      expect(findOneBySeatId).toHaveBeenCalledWith(mockFlightSeatDto.seat);
      expect(findByIdIncludeSpecificSeat).toHaveBeenCalledTimes(1);
      expect(findByFlightAndSeatClass).toHaveBeenCalledTimes(1);
    });

    it('should call findOneBySeatId method with expected param and throw not found exception', async () => {
      const findOneBySeatId = jest
        .spyOn(seatRepo, 'findOneBySeatId')
        .mockResolvedValue(null);

      const seatValidate = service.seatValidate(mockFlightSeatDto);
      await expect(seatValidate).rejects.toEqual(
        new NotFoundException('Seat data not found'),
      );
      expect(findOneBySeatId).toHaveBeenCalledTimes(1);
    });
  });

  describe('getFlightBaggagesById', () => {
    let mockFlight: Flight;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockBaggage: Baggage;

    beforeEach(async () => {
      mockBaggage = {
        id: faker.string.uuid(),
        flight: new Flight,
        capacity: faker.number.int({ min: 1 }),
        category: faker.helpers.arrayElement([
          'Carry on Item',
          'Checked Baggage',
          'Cabin Baggage',
        ]),
        chargePrice: faker.number.int({ min: 0 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.string.uuid(),
        code: faker.airline.airplane().iataTypeCode,
        registrationNumber: faker.string.alphanumeric(6),
        name: faker.airline.airplane().name,
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        totalSeat: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,

      };

      mockFlight = {
        id: faker.string.uuid(),
        code: faker.airline.flightNumber(),
        airplane: mockAirplane,
        origin: mockAirport,
        destination: mockAirport,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        boardingTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        baggage: [mockBaggage],
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return a flight with baggage info', async () => {
      // arrange
      const id = mockFlight.id;

      const findByIdIncludeBaggages = jest
        .spyOn(flightRepo, 'findByIdIncludeBaggages')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await service.getFlightBaggagesById(id);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(findByIdIncludeBaggages).toHaveBeenCalledTimes(1);
      expect(findByIdIncludeBaggages).toHaveBeenCalledWith(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.string.uuid()

      const findByIdIncludeBaggages = jest
        .spyOn(flightRepo, 'findByIdIncludeBaggages')
        .mockResolvedValue(null);

      // act
      const flight = service.getFlightBaggagesById(id);

      // assert
      await expect(flight).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(findByIdIncludeBaggages).toHaveBeenCalledTimes(1);
      expect(findByIdIncludeBaggages).toHaveBeenCalledWith(id);
    });
  });

  describe('getFlightSeatsById', () => {
    let mockFlight: Flight;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockSeat: Seat;
    let mockQuerySeatClass: SeatClassDto;

    beforeEach(async () => {
      mockSeat = {
        id: faker.string.uuid(),
        code: faker.string.alphanumeric(2),
        seatClass: faker.helpers.enumValue(ESeatClass),
        seatSide: faker.helpers.enumValue(ESeatSide),
        seatPosition: faker.helpers.enumValue(ESeatPosition),
        airplane: new Airplane,
        seqRow: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.string.uuid(),
        code: faker.airline.airplane().iataTypeCode,
        registrationNumber: faker.string.alphanumeric(6),
        name: faker.airline.airplane().name,
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        totalSeat: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
        seats: [mockSeat],
      };

      mockAirport = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.string.uuid(),
        code: faker.airline.flightNumber(),
        airplane: mockAirplane,
        origin: mockAirport,
        destination: mockAirport,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        boardingTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return a flight with seat info both economy and bussiness', async () => {
      // arrange
      const id = mockFlight.id;
      mockQuerySeatClass = {
        seatClass: null,
      };

      const findByIdIncludeSeats = jest
        .spyOn(flightRepo, 'findByIdIncludeSeats')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await service.getFlightSeatsById(id, mockQuerySeatClass);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(findByIdIncludeSeats).toHaveBeenCalledTimes(1);
      expect(findByIdIncludeSeats).toHaveBeenCalledWith(id, mockQuerySeatClass);
    });

    it('should return a flight with seat info both economy and bussiness', async () => {
      // arrange
      const id = mockFlight.id;
      mockQuerySeatClass = {
        seatClass: ESeatClass.ECONOMY,
      };

      mockSeat.seatClass = ESeatClass.ECONOMY;
      mockFlight.airplane.seats = [mockSeat];

      const findByIdIncludeSeats = jest
        .spyOn(flightRepo, 'findByIdIncludeSeats')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await service.getFlightSeatsById(id, mockQuerySeatClass);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(findByIdIncludeSeats).toHaveBeenCalledTimes(1);
      expect(findByIdIncludeSeats).toHaveBeenCalledWith(id, mockQuerySeatClass);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.string.uuid(),
        mockQuerySeatClass = {
          seatClass: null,
        };

      const findByIdIncludeSeats = jest
        .spyOn(flightRepo, 'findByIdIncludeSeats')
        .mockResolvedValue(null);

      // act
      const getFlightSeatsById = service.getFlightSeatsById(
        id,
        mockQuerySeatClass,
      );

      // assert
      await expect(getFlightSeatsById).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(findByIdIncludeSeats).toHaveBeenCalledTimes(1);
      expect(findByIdIncludeSeats).toHaveBeenCalledWith(id, mockQuerySeatClass);
    });
  });

  describe('getFlightAirportsById', () => {
    let mockFlight: Flight;
    let mockAirport: Airport;
    let mockAirplane: Airplane;

    beforeEach(async () => {
      mockAirplane = {
        id: faker.string.uuid(),
        code: faker.airline.airplane().iataTypeCode,
        registrationNumber: faker.string.alphanumeric(6),
        name: faker.airline.airplane().name,
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        totalSeat: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.string.uuid(),
        code: faker.airline.flightNumber(),
        airplane: mockAirplane,
        origin: mockAirport,
        destination: mockAirport,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        boardingTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return a flight with airport info', async () => {
      // arrange
      const id = mockFlight.id;

      const findByIdIncludeAirports = jest
        .spyOn(flightRepo, 'findByIdIncludeAirports')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await service.getFlightAirportsById(id);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(findByIdIncludeAirports).toHaveBeenCalledTimes(1);
      expect(findByIdIncludeAirports).toHaveBeenCalledWith(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.string.uuid()

      const findByIdIncludeAirports = jest
        .spyOn(flightRepo, 'findByIdIncludeAirports')
        .mockResolvedValue(null);

      // act
      const getFlightSeatsById = service.getFlightAirportsById(id);

      // assert
      await expect(getFlightSeatsById).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(findByIdIncludeAirports).toHaveBeenCalledTimes(1);
      expect(findByIdIncludeAirports).toHaveBeenCalledWith(id);
    });
  });
});
