import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { UnauthorizedException } from '@nestjs/common';
import { BasicStrategy } from '../auth/authbasic.strategy';

describe('BasicStrategy', () => {
  let app: TestingModule;
  let strategy: BasicStrategy;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      providers: [
        BasicStrategy,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              if (key === 'BASIC_USER') {
                return 'fww_internal_username';
              } else if (key === 'BASIC_PASS') {
                return '2331b08165edfca81d0c48cc8bd511aa';
              }
              return null;
            }),
          },
        },
      ],
    }).compile();

    strategy = app.get<BasicStrategy>(BasicStrategy);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should validate the correct username and password', async () => {
    // arrange
    const username = 'fww_internal_username';
    const password = 'fww_internal_password';

    // act
    const result = await strategy.validate(null, username, password);

    // assert
    expect(result).toBe(true);
  });

  it('should throw unauthorized exception for invalid username or password', async () => {
    // arrange
    const username = 'invalid_fww_internal_username';
    const password = 'invalid_fww_internal_password';

    // act
    const validate = strategy.validate(null, username, password);

    // assert
    await expect(validate).rejects.toThrow(new UnauthorizedException());
  });

  afterAll(async () => {
    await app.close();
  });
});
