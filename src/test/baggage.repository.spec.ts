import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { BaggageRepository } from '../repository/baggage.repository';
import { Baggage } from '../entities/baggage.entity';
import { Flight } from '../entities/flight.entity';

describe('BaggageRepository', () => {
  let repository: BaggageRepository;
  let mockBaggage: Baggage;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BaggageRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<BaggageRepository>(BaggageRepository);

    mockBaggage = {
      id: faker.string.uuid(),
      flight: new Flight,
      capacity: faker.number.int({ min: 1 }),
      category: faker.helpers.arrayElement([
        'Carry on Item',
        'Checked Baggage',
        'Cabin Baggage',
      ]),
      chargePrice: faker.number.int({ min: 0 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });
});
