import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { ReservationJourneyRepository } from '../repository/reservation-journey.repository';
import { Reservation } from '../entities/reservation.entity';
import { ReservationJourney } from '../entities/reservation-journey.entity';
import { EReservationStatus } from '../helpers/enum/reservation-status.enum';
import { IReservationJourney } from '../interface/reservation-new.interface';

describe('ReservationJourneyRepository', () => {
  let repository: ReservationJourneyRepository;
  let mockReservation: Reservation;
  let mockReservationJourney: ReservationJourney;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ReservationJourneyRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<ReservationJourneyRepository>(
      ReservationJourneyRepository,
    );

    mockReservation = {
      id: faker.string.uuid(),
      bookingCode: null,
      reservationCode: null,
      partnerId: faker.string.uuid(),
      memberId: faker.string.uuid(),
      flightDate: faker.date.future().toISOString().split('T')[0],
      flight: null,
      seat: null,
      priceActual: faker.number.int({ min: 500000 }),
      currentStatus: EReservationStatus.NEW,
      journeys: [mockReservationJourney],
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };

    mockReservationJourney = {
      id: faker.string.uuid(),
      reservation: mockReservation,
      description: EReservationStatus.NEW,
      journeyTime: faker.date.recent().toISOString(),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('write', () => {
    it('should return data reservation journey just made', async () => {
      // arrange
      const reservationJourneyData: IReservationJourney = {
        reservation: { id: mockReservation.id },
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
      };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockReservationJourney);

      // act
      const newReservationJourney = await repository.write(
        reservationJourneyData,
      );

      // assert
      expect(newReservationJourney).toEqual(mockReservationJourney);
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(reservationJourneyData);
    });
  });
});
