import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { PassengerRepository } from '../repository/passenger.repository';
import { Passenger } from '../entities/passenger.entity';
import { EIdentityCat } from '../helpers/enum/identity-category.enum';
import { IPassenger } from '../interface/passenger.interface';

describe('PassengerRepository', () => {
  let repository: PassengerRepository;
  let mockPassenger: Passenger;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PassengerRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<PassengerRepository>(PassengerRepository);

    mockPassenger = {
      id: faker.string.uuid(),
      identityCategory: EIdentityCat.KTP,
      identityNumber: faker.string.numeric({ length: 16 }),
      name: faker.person.fullName(),
      birthDate: faker.date
        .birthdate({ min: 10, max: 56, mode: 'age' })
        .toISOString()
        .split('T')[0],
      email: faker.internet.email(),
      phone: faker.phone.number(),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('getOne', () => {
    it('should return data passenger', async () => {
      // arrange
      const identityNumber = mockPassenger.identityNumber;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockPassenger);

      // act
      const passenger = await repository.getOne(identityNumber);

      // assert
      expect(passenger).toEqual(mockPassenger);
      expect(findOne).toHaveBeenCalledWith({ where: { identityNumber } });
    });

    it('should return null', async () => {
      // arrange
      const identityNumber = faker.string.numeric({ length: 16 });

      const findOne = jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      // act
      const passenger = await repository.getOne(identityNumber);

      // assert
      expect(passenger).toEqual(null);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({ where: { identityNumber } });
    });
  });

  describe('savePassenger', () => {
    it('should return data new passenger', async () => {
      // arrange
      const dataPassenger: IPassenger = {
        identityCategory: mockPassenger.identityCategory,
        identityNumber: mockPassenger.identityNumber,
        name: mockPassenger.name,
        birthDate: mockPassenger.birthDate,
      };

      const getOne = jest.spyOn(repository, 'getOne').mockResolvedValue(null);

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockPassenger);

      // act
      const passenger = await repository.savePassenger(dataPassenger);

      // assert
      expect(passenger).toEqual(mockPassenger);
      expect(getOne).toHaveBeenCalledWith(dataPassenger.identityNumber);
      expect(saveSpy).toHaveBeenCalledWith(dataPassenger);
    });

    it('should return data existing passenger', async () => {
      // arrange
      const dataPassenger: IPassenger = {
        identityCategory: mockPassenger.identityCategory,
        identityNumber: mockPassenger.identityNumber,
        name: mockPassenger.name,
        birthDate: mockPassenger.birthDate,
      };

      const getOne = jest
        .spyOn(repository, 'getOne')
        .mockResolvedValue(mockPassenger);

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockPassenger);

      // act
      const passenger = await repository.savePassenger(dataPassenger);

      // assert
      expect(passenger).toEqual(mockPassenger);
      expect(getOne).toHaveBeenCalledWith(dataPassenger.identityNumber);
    });
  });
});
