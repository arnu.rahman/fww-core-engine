import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker, fakerID_ID } from '@faker-js/faker';
import { FlightRepository } from '../repository/flight.repository';
import { Flight } from '../entities/flight.entity';
import { Airport } from '../entities/airport.entity';
import { Airplane } from '../entities/airplane.entity';
import { Baggage } from '../entities/baggage.entity';
import { Seat } from '../entities/seat.entity';
import { ESeatClass } from '../helpers/enum/seat-class.enum';
import { ESeatSide } from '../helpers/enum/seat-side.enum';
import { ESeatPosition } from '../helpers/enum/seat-position.enum';
import { EAirportCat } from '../helpers/enum/airport-category.enum';
import { EAirportClass } from '../helpers/enum/airport-class.enum';
import { ETimezone } from '../helpers/enum/timezone.enum';
import { SeatDto } from '../dto/core/seat.dto';
import { SeatClassDto } from '../dto/core/seat-class.dto';
import { FlightSeatDto } from '../dto/core/flight-seat.dto';
import { FilterFlightDto } from '../dto/core/filter-flight.dto';
import { FlightCandidateDto } from '../dto/core/flight-candidate.dto';

describe('FlightRepository', () => {
  let repository: FlightRepository;
  let mockFlight: Flight;
  let mockAirport: Airport;
  let mockAirplane: Airplane;
  let mockBaggage: Baggage;
  let mockSeat: Seat;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FlightRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<FlightRepository>(FlightRepository);

    mockBaggage = {
      id: faker.string.uuid(),
      flight: new Flight,
      capacity: faker.number.int({ min: 1 }),
      category: faker.helpers.arrayElement([
        'Carry on Item',
        'Checked Baggage',
        'Cabin Baggage',
      ]),
      chargePrice: faker.number.int({ min: 0 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };

    mockSeat = {
      id: faker.string.uuid(),
      code: faker.string.alphanumeric(2),
      seatClass: faker.helpers.enumValue(ESeatClass),
      seatSide: faker.helpers.enumValue(ESeatSide),
      seatPosition: faker.helpers.enumValue(ESeatPosition),
      airplane: new Airplane,
      seqRow: faker.number.int({ min: 1 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };

    mockAirplane = {
      id: faker.string.uuid(),
      code: faker.airline.airplane().iataTypeCode,
      registrationNumber: faker.string.alphanumeric(6),
      name: faker.airline.airplane().name,
      maxBusiness: faker.number.int({ min: 0 }),
      maxEconomy: faker.number.int({ min: 50 }),
      totalSeat: faker.number.int({ min: 1 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
      seats: [mockSeat],
    };

    mockAirport = {
      id: faker.string.uuid(),
      icao: faker.airline.airport().iataCode,
      iata: faker.airline.airport().iataCode,
      name: faker.airline.airport().name,
      airportCategory: EAirportCat.DOMESTIK,
      airportClass: EAirportClass.KELAS_III,
      manageBy: "Unit Penyelenggara Bandar Udara",
      address: "Kab. Kutai Barat, Kalimantan Timur",
      city: fakerID_ID.location.city(),
      timezone: ETimezone.WIB,
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };

    mockFlight = {
      id: faker.string.uuid(),
      code: faker.airline.flightNumber(),
      airplane: mockAirplane,
      origin: mockAirport,
      destination: mockAirport,
      departureTime: faker.date.anytime().toTimeString(),
      arrivalTime: faker.date.anytime().toTimeString(),
      boardingTime: faker.date.anytime().toTimeString(),
      durationInMinutes: faker.number.int({ min: 60 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
      baggage: [mockBaggage],
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('findByIdIncludeBaggages', () => {
    it('should return query data flight include baggages', async () => {
      // arrange
      const id = mockFlight.id;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await repository.findByIdIncludeBaggages(id);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        relations: ['baggage'],
        where: { id },
      });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.string.uuid()

      const findOne = jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      // act
      const flight = await repository.findByIdIncludeBaggages(id);

      // assert
      expect(flight).toEqual(null);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        relations: ['baggage'],
        where: { id },
      });
    });
  });

  describe('findByIdIncludeAirports', () => {
    it('should return query data flight include airport', async () => {
      // arrange
      delete mockFlight.baggage;

      const id = mockFlight.id;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await repository.findByIdIncludeAirports(id);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        relations: ['origin', 'destination', 'airplane'],
        where: { id },
      });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.string.uuid()

      const findOne = jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      // act
      const flight = await repository.findByIdIncludeAirports(id);

      // assert
      expect(flight).toEqual(null);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        relations: ['origin', 'destination', 'airplane'],
        where: { id },
      });
    });
  });

  describe('findByIdIncludeSeats', () => {
    it('should return data from method findByIdIncludeSeats with id', async () => {
      // arrange
      const createQueryBuilder: any = {
        innerJoinAndSelect: () => createQueryBuilder,
        where: () => createQueryBuilder,
        getOne: jest.fn().mockReturnValue(mockFlight)
      }
      jest.spyOn(repository, 'createQueryBuilder').mockImplementation(() => createQueryBuilder)
      const query: SeatClassDto = {
        seatClass: null
      }

      // act
      const getData = await repository.findByIdIncludeSeats(mockFlight.id, query);

      // assert
      expect(getData).toEqual(mockFlight);
    });

    it('should return data from method findByIdIncludeSeats with id and seat class', async () => {
      // arrange
      const createQueryBuilder: any = {
        innerJoinAndSelect: () => createQueryBuilder,
        where: () => createQueryBuilder,
        andWhere: () => createQueryBuilder,
        getOne: jest.fn().mockReturnValue(mockFlight)
      }
      jest.spyOn(repository, 'createQueryBuilder').mockImplementation(() => createQueryBuilder)
      const query: SeatClassDto = {
        seatClass: ESeatClass.ECONOMY
      }

      // act
      const getData = await repository.findByIdIncludeSeats(mockFlight.id, query);

      // assert
      expect(getData).toEqual(mockFlight);
    });
  })

  describe('findByIdIncludeSpecificSeat', () => {
    it('should return data from method findByIdIncludeSpecificSeat with params', async () => {
      // arrange
      const createQueryBuilder: any = {
        innerJoinAndSelect: () => createQueryBuilder,
        where: () => createQueryBuilder,
        andWhere: () => createQueryBuilder,
        getOne: jest.fn().mockReturnValue(mockFlight)
      }
      jest.spyOn(repository, 'createQueryBuilder').mockImplementation(() => createQueryBuilder)
      const flightSeatDto: FlightSeatDto = {
        flight: mockFlight.id,
        seat: mockSeat.id
      }

      // act
      const getData = await repository.findByIdIncludeSpecificSeat(flightSeatDto);

      // assert
      expect(getData).toEqual(mockFlight);
    });
  })

  describe('findWithQuery', () => {
    it('should return data from method findWithQuery', async () => {
      // arrange
      const createQueryBuilder: any = {
        innerJoinAndSelect: () => createQueryBuilder,
        getMany: jest.fn().mockReturnValue([mockFlight])
      }
      jest.spyOn(repository, 'createQueryBuilder').mockImplementation(() => createQueryBuilder)
      const query: FilterFlightDto = {
        origin: null,
        destination: null,
      }

      // act
      const getData = await repository.findWithQuery(query);

      // assert
      expect(getData).toEqual([mockFlight]);
    });

    it('should return data from method findWithQuery with origin', async () => {
      // arrange
      const createQueryBuilder: any = {
        innerJoinAndSelect: () => createQueryBuilder,
        where: () => createQueryBuilder,
        andWhere: () => createQueryBuilder,
        orWhere: () => createQueryBuilder,
        getMany: jest.fn().mockReturnValue([mockFlight])
      }
      jest.spyOn(repository, 'createQueryBuilder').mockImplementation(() => createQueryBuilder)
      const query: FilterFlightDto = {
        origin: mockFlight.id,
        destination: null,
      }

      // act
      const getData = await repository.findWithQuery(query);

      // assert
      expect(getData).toEqual([mockFlight]);
    });

    it('should return data from method findWithQuery with destination', async () => {
      // arrange
      const createQueryBuilder: any = {
        innerJoinAndSelect: () => createQueryBuilder,
        where: () => createQueryBuilder,
        andWhere: () => createQueryBuilder,
        orWhere: () => createQueryBuilder,
        getMany: jest.fn().mockReturnValue([mockFlight])
      }
      jest.spyOn(repository, 'createQueryBuilder').mockImplementation(() => createQueryBuilder)
      const query: FilterFlightDto = {
        origin: null,
        destination: mockFlight.id,
      }

      // act
      const getData = await repository.findWithQuery(query);

      // assert
      expect(getData).toEqual([mockFlight]);
    });
  })

  describe('findCandidate', () => {
    it('should return data from method findCandidate with params', async () => {
      // arrange
      const createQueryBuilder: any = {
        innerJoinAndSelect: () => createQueryBuilder,
        where: () => createQueryBuilder,
        andWhere: () => createQueryBuilder,
        getMany: jest.fn().mockReturnValue([mockFlight])
      }
      jest.spyOn(repository, 'createQueryBuilder').mockImplementation(() => createQueryBuilder)
      const criteria: FlightCandidateDto = {
        originAirportId: mockFlight.id,
        destinationAirportId: mockFlight.id
      }

      // act
      const getData = await repository.findCandidate(criteria);

      // assert
      expect(getData).toEqual([mockFlight]);
    });
  })
});
