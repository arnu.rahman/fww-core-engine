import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { SeatRepository } from '../repository/seat.repository';
import { Seat } from '../entities/seat.entity';
import { ESeatClass } from '../helpers/enum/seat-class.enum';
import { ESeatSide } from '../helpers/enum/seat-side.enum';
import { ESeatPosition } from '../helpers/enum/seat-position.enum';
import { Airplane } from '../entities/airplane.entity';

describe('SeatRepository', () => {
  let repository: SeatRepository;
  let mockSeat: Seat;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SeatRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<SeatRepository>(SeatRepository);

    mockSeat = {
      id: faker.string.uuid(),
      code: faker.string.alphanumeric(2),
      seatClass: faker.helpers.enumValue(ESeatClass),
      seatSide: faker.helpers.enumValue(ESeatSide),
      seatPosition: faker.helpers.enumValue(ESeatPosition),
      airplane: new Airplane,
      seqRow: faker.number.int({ min: 1 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('findOneBySeatId', () => {
    it('should return data seat', async () => {
      // arrange
      const id = mockSeat.id;

      const findOneBy = jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValue(mockSeat);

      // act
      const seat = await repository.findOneBySeatId(id);

      // assert
      expect(seat).toEqual(mockSeat);
      expect(findOneBy).toBeCalledTimes(1);
      expect(findOneBy).toHaveBeenCalledWith({ id });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.string.uuid()

      const findOneBy = jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValue(null);

      // act
      const seat = await repository.findOneBySeatId(id);

      // assert
      expect(seat).toEqual(null);
      expect(findOneBy).toBeCalledTimes(1);
      expect(findOneBy).toHaveBeenCalledWith({ id });
    });
  });
});
