import { Test, TestingModule } from "@nestjs/testing";
import { faker } from "@faker-js/faker";
import { ForbiddenException } from '@nestjs/common';
import { InquiryController } from "../controllers/inqury.controller";
import { InquiryService } from "../services/inquiry.service";
import { FilterAirportDto } from "../dto/core/filter-airport.dto";
import { FilterFlightDto } from "../dto/core/filter-flight.dto";
import { FlightCandidateDto } from "../dto/core/flight-candidate.dto";
import { FlightSeatDto } from "../dto/core/flight-seat.dto";
import { SeatClassDto } from "../dto/core/seat-class.dto";

describe("InquiryController", () => {
  let controller: InquiryController;
  let spyService: InquiryService

  beforeAll(async () => {
    const ApiServiceProvider = {
      provide: InquiryService,
      useFactory: () => ({
        searchAirport: jest.fn(() => []),
        filterFlight: jest.fn(() => []),
        searchFlight: jest.fn(() => { }),
        seatValidate: jest.fn(() => { }),
        getFlightSeatsById: jest.fn(() => { }),
        getFlightBaggagesById: jest.fn(() => { }),
        getFlightAirportsById: jest.fn(() => { }),
      })
    }
    const app: TestingModule = await Test.createTestingModule({
      controllers: [InquiryController],
      providers: [InquiryService, ApiServiceProvider],
    }).compile();

    controller = app.get<InquiryController>(InquiryController);
    spyService = app.get<InquiryService>(InquiryService);
  })

  describe('getAirport', () => {
    it("calling getAirport method", () => {
      const dto = new FilterAirportDto;
      expect(controller.getAirport(dto)).not.toEqual(null);
    })

    it('should throw forbidden exception when getAirport', async () => {
      // arrange
      const dto = new FilterAirportDto
      const serviceSpy = jest
        .spyOn(spyService, 'searchAirport')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.getAirport(dto);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(dto);
    });
  })

  describe('getFlight', () => {
    it("calling getFlight method", () => {
      const dto = new FilterFlightDto;
      expect(controller.getFlight(dto)).not.toEqual(null);
    })

    it('should throw forbidden exception when getFlight', async () => {
      // arrange
      const dto = new FilterFlightDto
      const serviceSpy = jest
        .spyOn(spyService, 'filterFlight')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.getFlight(dto);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(dto);
    });
  })

  describe('getFlightCandidate', () => {
    it("calling getFlightCandidate method", () => {
      const dto = new FlightCandidateDto;
      expect(controller.getFlightCandidate(dto)).not.toEqual(null);
    })

    it('should throw forbidden exception when getFlightCandidate', async () => {
      // arrange
      const dto = new FlightCandidateDto
      const serviceSpy = jest
        .spyOn(spyService, 'searchFlight')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.getFlightCandidate(dto);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(dto);
    });
  })

  describe('seatValidate', () => {
    it("calling seatValidate method", () => {
      const dto = new FlightSeatDto;
      expect(controller.seatValidate(dto)).not.toEqual(null);
    })

    it('should throw forbidden exception when seatValidate', async () => {
      // arrange
      const dto = new FlightSeatDto
      const serviceSpy = jest
        .spyOn(spyService, 'seatValidate')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.seatValidate(dto);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(dto);
    });
  })

  describe('getFlightSeatsById', () => {
    it("calling getFlightSeatsById method", () => {
      const dto = new SeatClassDto;
      const id = faker.string.uuid()
      expect(controller.getFlightSeatsById(id, dto)).not.toEqual(null);
    })

    it('should throw forbidden exception when getFlightSeatsById', async () => {
      // arrange
      const dto = new SeatClassDto
      const id = faker.string.uuid()
      const serviceSpy = jest
        .spyOn(spyService, 'getFlightSeatsById')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.getFlightSeatsById(id,dto);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(id,dto);
    });
  })

  describe('getFlightBaggagesById', () => {
    it("calling getFlightBaggagesById method", () => {
      const id = faker.string.uuid()
      expect(controller.getFlightBaggagesById(id)).not.toEqual(null);
    })

    it('should throw forbidden exception when getFlightBaggagesById', async () => {
      // arrange
      const id = faker.string.uuid()
      const serviceSpy = jest
        .spyOn(spyService, 'getFlightBaggagesById')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.getFlightBaggagesById(id);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(id);
    });
  })

  describe('getFlightAirportsById', () => {
    it("calling getFlightAirportsById method", () => {
      const id = faker.string.uuid()
      expect(controller.getFlightAirportsById(id)).not.toEqual(null);
    })

    it('should throw forbidden exception when getFlightAirportsById', async () => {
      // arrange
      const id = faker.string.uuid()
      const serviceSpy = jest
        .spyOn(spyService, 'getFlightAirportsById')
        .mockRejectedValue(
          new ForbiddenException('Forbidden'),
        );

      // act
      const controllerSpy = controller.getFlightAirportsById(id);

      // assert
      await expect(controllerSpy).rejects.toEqual(
        new ForbiddenException('Forbidden'),
      );
      expect(serviceSpy).toHaveBeenCalledTimes(2);
      expect(serviceSpy).toHaveBeenCalledWith(id);
    });
  })
})
