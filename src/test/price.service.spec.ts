import { Test, TestingModule } from "@nestjs/testing";
import { faker, fakerID_ID } from '@faker-js/faker';
import { NotFoundException } from '@nestjs/common';

import { FlightRepository } from "../repository/flight.repository";
import { EAirportCat } from "../helpers/enum/airport-category.enum";
import { EAirportClass } from "../helpers/enum/airport-class.enum";
import { ESeatClass } from "../helpers/enum/seat-class.enum";
import { Airplane } from "../entities/airplane.entity";
import { Flight } from "../entities/flight.entity";
import { ETimezone } from "../helpers/enum/timezone.enum";
import { PriceService } from "../services/price.service";
import { PriceRepository } from "../repository/price.repository";
import { CreatePriceDto } from "../dto/request/create-price.dto";
import { UpdatePriceDto } from "../dto/request/update-price.dto";

describe('PriceService', () => {
  let service: PriceService;

  const priceRepo = {
    save: jest.fn(),
    update: jest.fn(),
    findOne: jest.fn(),
    find: jest.fn()
  };

  const flightRepo = {
    findOne: jest.fn()
  }

  const mockBaggage = {
    id: faker.string.uuid(),
    flight: new Flight,
    capacity: faker.number.int({ min: 1 }),
    category: faker.helpers.arrayElement([
      'Carry on Item',
      'Checked Baggage',
      'Cabin Baggage',
    ]),
    chargePrice: faker.number.int({ min: 0 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  const mockPrice = {
    id: faker.string.uuid(),
    seatClass: faker.helpers.enumValue(ESeatClass),
    price: faker.number.int({ min: 500000 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  const mockAirplane: Airplane = {
    id: faker.string.uuid(),
    code: faker.airline.airplane().iataTypeCode,
    registrationNumber: faker.string.alphanumeric(6),
    name: faker.airline.airplane().name,
    maxBusiness: faker.number.int({ min: 0 }),
    maxEconomy: faker.number.int({ min: 50 }),
    totalSeat: faker.number.int({ min: 1 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  const mockAirport = {
    id: faker.string.uuid(),
    icao: faker.airline.airport().iataCode,
    iata: faker.airline.airport().iataCode,
    name: faker.airline.airport().name,
    airportCategory: EAirportCat.DOMESTIK,
    airportClass: EAirportClass.KELAS_III,
    manageBy: "Unit Penyelenggara Bandar Udara",
    address: "Kab. Kutai Barat, Kalimantan Timur",
    city: fakerID_ID.location.city(),
    timezone: ETimezone.WIB,
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  const mockFlight = {
    id: faker.string.uuid(),
    code: faker.airline.flightNumber(),
    airplane: mockAirplane,
    origin: mockAirport,
    destination: mockAirport,
    departureTime: faker.date.anytime().toTimeString(),
    arrivalTime: faker.date.anytime().toTimeString(),
    boardingTime: faker.date.anytime().toTimeString(),
    durationInMinutes: faker.number.int({ min: 60 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
    baggages: [mockBaggage],
    prices: [mockPrice],
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PriceService,
        { provide: PriceRepository, useValue: priceRepo },
        { provide: FlightRepository, useValue: flightRepo },
      ],
    }).compile();
    module.useLogger(false);
    service = module.get<PriceService>(PriceService);
  });

  afterEach(() => jest.clearAllMocks());

  it('should call create method with expected params', async () => {
    const id = mockFlight.id
    const isFlightExist = jest
      .spyOn(service, 'isFlightExist')
      .mockResolvedValue(mockFlight);
    const createSpy = jest.spyOn(service, 'create');
    const dto: CreatePriceDto = {
      flight: mockFlight,
      seatClass: mockPrice.seatClass,
      price: mockPrice.price
    };
    service.create(dto);
    expect(createSpy).toHaveBeenCalledWith(dto);
    expect(isFlightExist).toHaveBeenCalledTimes(1);
    expect(isFlightExist).toHaveBeenCalledWith(id);
  });

  it('should call update method with expected params', async () => {
    const id = mockPrice.id
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockPrice);
    jest
      .spyOn(service, 'isFlightExist')
      .mockResolvedValue(mockFlight);
    const updateSpy = jest.spyOn(service, 'update');
    const dto: UpdatePriceDto = {
      flight: mockFlight,
      seatClass: mockPrice.seatClass,
      price: mockPrice.price
    };
    service.update(id, dto);
    expect(updateSpy).toHaveBeenCalledWith(id, dto);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call remove method with expected params', async () => {
    const id = mockAirport.id
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockPrice);
    const removeSpy = jest.spyOn(service, 'remove');
    service.remove(id);
    expect(removeSpy).toHaveBeenCalledWith(id);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call findOne method with expected param', async () => {
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockPrice);
    const findOneSpy = jest.spyOn(service, 'findOne');
    const id = mockPrice.id
    service.findOne(id);
    expect(findOneSpy).toHaveBeenCalledWith(id);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call findAll method with expected param', async () => {
    const findAllSpy = jest.spyOn(service, 'findAll');
    service.findAll();
    expect(findAllSpy).toHaveBeenCalledWith();
  });

  it('should call isExist method with expected param', async () => {
    const findOne = jest
      .spyOn(priceRepo, 'findOne')
      .mockResolvedValue(mockPrice);

    const isExistSpy = jest.spyOn(service, 'isExist');
    const id = mockPrice.id
    service.isExist(id);
    expect(isExistSpy).toHaveBeenCalledWith(id);
    expect(findOne).toHaveBeenCalledTimes(1);
  });

  it('should call isExist method with expected param and throw not found exception', async () => {
    const findOne = jest
      .spyOn(priceRepo, 'findOne')
      .mockResolvedValue(null);

    const id = mockBaggage.id
    const isExist = service.isExist(id);
    await expect(isExist).rejects.toEqual(
      new NotFoundException('Price not found'),
    );
    expect(findOne).toHaveBeenCalledTimes(1);
  });

  it('should call isFLightExist method with expected param', async () => {
    const findOne = jest
      .spyOn(flightRepo, 'findOne')
      .mockResolvedValue(mockFlight);

    const isExistSpy = jest.spyOn(service, 'isFlightExist');
    const id = mockFlight.id
    service.isFlightExist(id);
    expect(isExistSpy).toHaveBeenCalledWith(id);
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({ "where": { "id": id } });
  });

  it('should call isFLightExist method with expected param and throw not found exception', async () => {
    const findOne = jest
      .spyOn(flightRepo, 'findOne')
      .mockResolvedValue(null);

    const id = mockFlight.id
    const isExist = service.isFlightExist(id);
    await expect(isExist).rejects.toEqual(
      new NotFoundException('Flight not found'),
    );
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({ "where": { "id": id } });
  });
})
