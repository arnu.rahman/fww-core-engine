import { Test, TestingModule } from "@nestjs/testing";
import { faker, fakerID_ID } from '@faker-js/faker';
import { NotFoundException } from '@nestjs/common';

import { CreateAirportDto } from "../dto/request/create-airport.dto";
import { AirportRepository } from "../repository/airport.repository";
import { AirportService } from "../services/airport.service";
import { UpdateAirportDto } from "../dto/request/update-airport.dto";
import { EAirportCat } from "../helpers/enum/airport-category.enum";
import { EAirportClass } from "../helpers/enum/airport-class.enum";

describe('AirportService', () => {
  let service: AirportService;

  const airportRepo = {
    findWithQuery: jest.fn(),
    save: jest.fn(),
    update: jest.fn(),
    findOne: jest.fn(),
    find: jest.fn()
  };

  const mockAirport = {
    id: faker.string.uuid(),
    icao: faker.airline.airport().iataCode,
    iata: faker.airline.airport().iataCode,
    name: faker.airline.airport().name,
    airportCategory: EAirportCat.DOMESTIK,
    airportClass: EAirportClass.KELAS_III,
    manageBy: "Unit Penyelenggara Bandar Udara",
    address: "Kab. Kutai Barat, Kalimantan Timur",
    city: fakerID_ID.location.city(),
    timezone: faker.location.timeZone(),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AirportService,
        { provide: AirportRepository, useValue: airportRepo },
      ],
    }).compile();
    module.useLogger(false);
    service = module.get<AirportService>(AirportService);
  });

  afterEach(() => jest.clearAllMocks());

  it('should call create method with expected params', async () => {
    const createSpy = jest.spyOn(service, 'create');
    const dto = new CreateAirportDto();
    service.create(dto);
    expect(createSpy).toHaveBeenCalledWith(dto);
  });

  it('should call update method with expected params', async () => {
    const id = mockAirport.id
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockAirport);
    const updateSpy = jest.spyOn(service, 'update');
    const dto = new UpdateAirportDto();
    service.update(id, dto);
    expect(updateSpy).toHaveBeenCalledWith(id, dto);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call remove method with expected params', async () => {
    const id = mockAirport.id
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockAirport);
    const removeSpy = jest.spyOn(service, 'remove');
    service.remove(id);
    expect(removeSpy).toHaveBeenCalledWith(id);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call findOne method with expected param', async () => {
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockAirport);
    const findOneSpy = jest.spyOn(service, 'findOne');
    const id = mockAirport.id
    service.findOne(id);
    expect(findOneSpy).toHaveBeenCalledWith(id);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call findAll method with expected param', async () => {
    const findAllSpy = jest.spyOn(service, 'findAll');
    service.findAll();
    expect(findAllSpy).toHaveBeenCalledWith();
  });

  it('should call isExist method with expected param', async () => {
    const findOne = jest
        .spyOn(airportRepo, 'findOne')
        .mockResolvedValue(mockAirport);

    const isExistSpy = jest.spyOn(service, 'isExist');
    const id = mockAirport.id
    service.isExist(id);
    expect(isExistSpy).toHaveBeenCalledWith(id);
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({"where": {"id":id}});
  });

  it('should call isExist method with expected param and throw not found exception', async () => {
    const findOne = jest
        .spyOn(airportRepo, 'findOne')
        .mockResolvedValue(null);

    const id = mockAirport.id
    const isExist = service.isExist(id);
    await expect(isExist).rejects.toEqual(
      new NotFoundException('Airport not found'),
    );
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({"where": {"id":id}});
  });
})
