import { Test, TestingModule } from "@nestjs/testing";
import { faker } from '@faker-js/faker';
import { NotFoundException } from '@nestjs/common';

import { ESeatClass } from "../helpers/enum/seat-class.enum";
import { Airplane } from "../entities/airplane.entity";
import { SeatService } from "../services/seat.service";
import { SeatRepository } from "../repository/seat.repository";
import { CreateSeatDto } from "../dto/request/create-seat.dto";
import { UpdateSeatDto } from "../dto/request/update-seat.dto";
import { Seat } from "../entities/seat.entity";
import { ESeatSide } from "../helpers/enum/seat-side.enum";
import { ESeatPosition } from "../helpers/enum/seat-position.enum";
import { AirplaneRepository } from "../repository/airplane.repository";

describe('SeatService', () => {
  let service: SeatService;

  const seatRepo = {
    save: jest.fn(),
    update: jest.fn(),
    findOne: jest.fn(),
    find: jest.fn()
  };

  const airplaneRepo = {
    findOne: jest.fn()
  }

  const mockSeat: Seat = {
    id: faker.string.uuid(),
    code: faker.string.alphanumeric(2),
    seatClass: faker.helpers.enumValue(ESeatClass),
    seatSide: faker.helpers.enumValue(ESeatSide),
    seatPosition: faker.helpers.enumValue(ESeatPosition),
    airplane: new Airplane,
    seqRow: faker.number.int({ min: 1 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  const mockAirplane: Airplane = {
    id: faker.string.uuid(),
    code: faker.airline.airplane().iataTypeCode,
    registrationNumber: faker.string.alphanumeric(6),
    name: faker.airline.airplane().name,
    maxBusiness: faker.number.int({ min: 0 }),
    maxEconomy: faker.number.int({ min: 50 }),
    totalSeat: faker.number.int({ min: 1 }),
    createdAt: faker.date.recent().toISOString(),
    updatedAt: faker.date.recent().toISOString(),
    deletedAt: null,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SeatService,
        { provide: SeatRepository, useValue: seatRepo },
        { provide: AirplaneRepository, useValue: airplaneRepo },
      ],
    }).compile();
    module.useLogger(false);
    service = module.get<SeatService>(SeatService);
  });

  afterEach(() => jest.clearAllMocks());

  it('should call create method with expected params', async () => {
    const id = mockAirplane.id
    const isAirplaneExist = jest
      .spyOn(service, 'isAirplaneExist')
      .mockResolvedValue(mockAirplane);
    const createSpy = jest.spyOn(service, 'create');
    const dto: CreateSeatDto = {
      airplane: mockAirplane,
      code: mockSeat.code,
      seatClass: mockSeat.seatClass,
      seatSide: mockSeat.seatSide,
      seatPosition: mockSeat.seatPosition,
      seqRow: mockSeat.seqRow
    };
    service.create(dto);
    expect(createSpy).toHaveBeenCalledWith(dto);
    expect(isAirplaneExist).toHaveBeenCalledTimes(1);
    expect(isAirplaneExist).toHaveBeenCalledWith(id);
  });

  it('should call update method with expected params', async () => {
    const id = mockSeat.id
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockSeat);
    jest
      .spyOn(service, 'isAirplaneExist')
      .mockResolvedValue(mockAirplane);
    const updateSpy = jest.spyOn(service, 'update');
    const dto: UpdateSeatDto = {
      airplane: mockAirplane,
      code: mockSeat.code,
      seatClass: mockSeat.seatClass,
      seatSide: mockSeat.seatSide,
      seatPosition: mockSeat.seatPosition,
      seqRow: mockSeat.seqRow
    };
    service.update(id, dto);
    expect(updateSpy).toHaveBeenCalledWith(id, dto);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call remove method with expected params', async () => {
    const id = mockSeat.id
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockSeat);
    const removeSpy = jest.spyOn(service, 'remove');
    service.remove(id);
    expect(removeSpy).toHaveBeenCalledWith(id);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call findOne method with expected param', async () => {
    const isExist = jest
      .spyOn(service, 'isExist')
      .mockResolvedValue(mockSeat);
    const findOneSpy = jest.spyOn(service, 'findOne');
    const id = mockSeat.id
    service.findOne(id);
    expect(findOneSpy).toHaveBeenCalledWith(id);
    expect(isExist).toHaveBeenCalledTimes(1);
    expect(isExist).toHaveBeenCalledWith(id);
  });

  it('should call findAll method with expected param', async () => {
    const findAllSpy = jest.spyOn(service, 'findAll');
    service.findAll();
    expect(findAllSpy).toHaveBeenCalledWith();
  });

  it('should call isExist method with expected param', async () => {
    const findOne = jest
      .spyOn(seatRepo, 'findOne')
      .mockResolvedValue(mockSeat);

    const isExistSpy = jest.spyOn(service, 'isExist');
    const id = mockSeat.id
    service.isExist(id);
    expect(isExistSpy).toHaveBeenCalledWith(id);
    expect(findOne).toHaveBeenCalledTimes(1);
  });

  it('should call isExist method with expected param and throw not found exception', async () => {
    const findOne = jest
      .spyOn(seatRepo, 'findOne')
      .mockResolvedValue(null);

    const id = mockSeat.id
    const isExist = service.isExist(id);
    await expect(isExist).rejects.toEqual(
      new NotFoundException('Seat not found'),
    );
    expect(findOne).toHaveBeenCalledTimes(1);
  });

  it('should call isAirplaneExist method with expected param', async () => {
    const findOne = jest
      .spyOn(airplaneRepo, 'findOne')
      .mockResolvedValue(mockAirplane);

    const isExistSpy = jest.spyOn(service, 'isAirplaneExist');
    const id = mockAirplane.id
    service.isAirplaneExist(id);
    expect(isExistSpy).toHaveBeenCalledWith(id);
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({ "where": { "id": id } });
  });

  it('should call isAirplaneExist method with expected param and throw not found exception', async () => {
    const findOne = jest
      .spyOn(airplaneRepo, 'findOne')
      .mockResolvedValue(null);

    const id = mockAirplane.id
    const isExist = service.isAirplaneExist(id);
    await expect(isExist).rejects.toEqual(
      new NotFoundException('Airplane not found'),
    );
    expect(findOne).toHaveBeenCalledTimes(1);
    expect(findOne).toHaveBeenCalledWith({ "where": { "id": id } });
  });
})
