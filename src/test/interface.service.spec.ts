import { Test, TestingModule } from "@nestjs/testing";
import { InterfaceService } from "../services/interface.service";
import { ETimezone } from "../helpers/enum/timezone.enum";

describe('InterfaceService', () => {
  let service: InterfaceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [InterfaceService],
    }).compile();
    service = module.get<InterfaceService>(InterfaceService);
  });

  afterEach(() => jest.clearAllMocks());

  it('should call getTimezone method with airportTz WIB', async () => {
    const dataSpy = jest.spyOn(service, 'getTimezone');
    const airportTz = ETimezone.WIB
    service.getTimezone(airportTz);
    expect(dataSpy).toHaveBeenCalledWith(airportTz);
  });

  it('should call getTimezone method with airportTz WITA', async () => {
    const dataSpy = jest.spyOn(service, 'getTimezone');
    const airportTz = ETimezone.WITA
    service.getTimezone(airportTz);
    expect(dataSpy).toHaveBeenCalledWith(airportTz);
  });

  it('should call getTimezone method with airportTz WIT', async () => {
    const dataSpy = jest.spyOn(service, 'getTimezone');
    const airportTz = ETimezone.WIT
    service.getTimezone(airportTz);
    expect(dataSpy).toHaveBeenCalledWith(airportTz);
  });
})
