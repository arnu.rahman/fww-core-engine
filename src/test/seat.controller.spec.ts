import { Test, TestingModule } from "@nestjs/testing";
import { SeatController } from "../controllers/seat.controller";
import { SeatService } from "../services/seat.service";
import { CreateSeatDto } from "../dto/request/create-seat.dto";
import { UpdateSeatDto } from "../dto/request/update-seat.dto";
import { faker } from "@faker-js/faker";
import { ForbiddenException } from '@nestjs/common';

describe("SeatController", () => {
  let controller: SeatController;
  let spyService: SeatService

  beforeAll(async () => {
    const ApiServiceProvider = {
      provide: SeatService,
      useFactory: () => ({
        create: jest.fn(() => []),
        update: jest.fn(() => []),
        remove: jest.fn(() => { }),
        findAll: jest.fn(() => { }),
        findOne: jest.fn(() => { })
      })
    }
    const app: TestingModule = await Test.createTestingModule({
      controllers: [SeatController],
      providers: [SeatService, ApiServiceProvider],
    }).compile();

    controller = app.get<SeatController>(SeatController);
    spyService = app.get<SeatService>(SeatService);
  })

  it("calling create method", () => {
    const dto = new CreateSeatDto();
    expect(controller.create(dto)).not.toEqual(null);
  })

  it('should throw forbidden exception when create', async () => {
    // arrange
    const dto = new CreateSeatDto()
    const createService = jest
      .spyOn(spyService, 'create')
      .mockRejectedValue(
        new ForbiddenException('Forbidden'),
      );

    // act
    const createCont = controller.create(dto);

    // assert
    await expect(createCont).rejects.toEqual(
      new ForbiddenException('Forbidden'),
    );
    expect(createService).toHaveBeenCalledTimes(2);
    expect(createService).toHaveBeenCalledWith(dto);
  });

  it("calling update method", () => {
    const id = faker.string.uuid();
    const dto = new UpdateSeatDto();
    controller.update(id,dto);
    expect(spyService.update).toHaveBeenCalled();
    expect(spyService.update).toHaveBeenCalledWith(id,dto);
  })

  it('should throw forbidden exception when update', async () => {
    // arrange
    const id = faker.string.uuid();
    const dto = new UpdateSeatDto()
    const updateService = jest
      .spyOn(spyService, 'update')
      .mockRejectedValue(
        new ForbiddenException('Forbidden'),
      );

    // act
    const updateCont = controller.update(id,dto);

    // assert
    await expect(updateCont).rejects.toEqual(
      new ForbiddenException('Forbidden'),
    );
    expect(updateService).toHaveBeenCalledTimes(2);
    expect(updateService).toHaveBeenCalledWith(id, dto);
  });

  it("calling remove method", () => {
    const id = faker.string.uuid();
    controller.remove(id);
    expect(spyService.remove).toHaveBeenCalled();
    expect(spyService.remove).toHaveBeenCalledWith(id);
  })

  it('should throw forbidden exception when remove', async () => {
    // arrange
    const id = faker.string.uuid();
    const deleteService = jest
      .spyOn(spyService, 'remove')
      .mockRejectedValue(
        new ForbiddenException('Forbidden'),
      );

    // act
    const deleteCont = controller.remove(id);

    // assert
    await expect(deleteCont).rejects.toEqual(
      new ForbiddenException('Forbidden'),
    );
    expect(deleteService).toHaveBeenCalledTimes(2);
    expect(deleteService).toHaveBeenCalledWith(id);
  });

  it("calling findAll method", () => {
    controller.findAll();
    expect(spyService.findAll).toHaveBeenCalled();
  })

  it('should throw forbidden exception when findAll', async () => {
    // arrange
    const findAllService = jest
      .spyOn(spyService, 'findAll')
      .mockRejectedValue(
        new ForbiddenException('Forbidden'),
      );

    // act
    const findAllCont = controller.findAll();

    // assert
    await expect(findAllCont).rejects.toEqual(
      new ForbiddenException('Forbidden'),
    );
    expect(findAllService).toHaveBeenCalledTimes(2);
    expect(findAllService).toHaveBeenCalled();
  });

  it("calling findOneById method", () => {
    const id = faker.string.uuid();
    controller.findOne(id);
    expect(spyService.findOne).toHaveBeenCalled();
  })

  it('should throw forbidden exception when findOne', async () => {
    // arrange
    const id = faker.string.uuid();
    const findOneService = jest
      .spyOn(spyService, 'findOne')
      .mockRejectedValue(
        new ForbiddenException('Forbidden'),
      );

    // act
    const findOneCont = controller.findOne(id);

    // assert
    await expect(findOneCont).rejects.toEqual(
      new ForbiddenException('Forbidden'),
    );
    expect(findOneService).toHaveBeenCalledTimes(2);
    expect(findOneService).toHaveBeenCalledWith(id);
  });
})
