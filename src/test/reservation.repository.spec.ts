import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker, fakerID_ID } from '@faker-js/faker';
import {
  IReservationJourney,
  IReservationNew,
} from '../interface/reservation-new.interface';
import { IGenericId } from '../interface/generic-id.interface';
import { NotFoundException } from '@nestjs/common';
import { ReservationRepository } from '../repository/reservation.repository';
import { Reservation } from '../entities/reservation.entity';
import { EReservationStatus } from '../helpers/enum/reservation-status.enum';
import { ReservationJourney } from '../entities/reservation-journey.entity';
import { IPassenger } from '../interface/passenger.interface';
import { EIdentityCat } from '../helpers/enum/identity-category.enum';
import { IReservationUpdateData } from '../interface/reservation-update.interface';
import { Passenger } from '../entities/passenger.entity';
import { Airport } from '../entities/airport.entity';
import { Airplane } from '../entities/airplane.entity';
import { Flight } from '../entities/flight.entity';
import { Seat } from '../entities/seat.entity';
import { EAirportCat } from '../helpers/enum/airport-category.enum';
import { EAirportClass } from '../helpers/enum/airport-class.enum';
import { ETimezone } from '../helpers/enum/timezone.enum';
import { ESeatClass } from '../helpers/enum/seat-class.enum';
import { ESeatSide } from '../helpers/enum/seat-side.enum';
import { ESeatPosition } from '../helpers/enum/seat-position.enum';

describe('ReservationRepository', () => {
  let repository: ReservationRepository;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ReservationRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<ReservationRepository>(ReservationRepository);
  });

  afterEach(() => jest.clearAllMocks());

  describe('findReservationById', () => {
    let mockReservation: Reservation;

    beforeEach(async () => {
      mockReservation = {
        id: faker.string.uuid(),
        bookingCode: null,
        reservationCode: null,
        partnerId: faker.string.uuid(),
        memberId: faker.string.uuid(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: null,
        seat: null,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data reservation', async () => {
      // arrange
      const id = mockReservation.id;

      const findOneBy = jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await repository.findReservationById(id);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findOneBy).toHaveBeenCalledWith({ id });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.string.uuid()

      const findOneBy = jest
        .spyOn(repository, 'findOneBy')
        .mockResolvedValue(null);

      // act
      const reservation = await repository.findReservationById(id);

      // assert
      expect(reservation).toEqual(null);
      expect(findOneBy).toBeCalledTimes(1);
      expect(findOneBy).toHaveBeenCalledWith({ id });
    });
  });

  describe('saveReservation', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.string.uuid(),
        reservation: mockReservation,
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.string.uuid(),
        bookingCode: faker.string.alphanumeric({ length: 8, casing: 'upper' }),
        reservationCode: faker.string.alphanumeric({
          length: 6,
          casing: 'upper',
        }),
        partnerId: faker.string.uuid(),
        memberId: faker.string.uuid(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: null,
        seat: null,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data new reservation with its journey just made', async () => {
      // arrange
      const dataPassenger: IPassenger = {
        identityCategory: EIdentityCat.KTP,
        identityNumber: faker.string.numeric({ length: 16 }),
      };

      const genericId: IGenericId = {
        id: faker.string.uuid(),
      };

      const dataReservationJourney: IReservationJourney = {
        description: mockReservationJourney.description,
        journeyTime: mockReservationJourney.journeyTime,
      };

      const dataReservation: IReservationNew = {
        partnerId: faker.string.uuid(),
        memberId: faker.string.uuid(),
        passenger: dataPassenger,
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: genericId,
        seat: genericId,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [dataReservationJourney],
      };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await repository.saveReservation(dataReservation);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(dataReservation);
    });
  });
  describe('updateReservation', () => {
    let mockReservation: Reservation;

    beforeEach(async () => {
      mockReservation = {
        id: faker.string.uuid(),
        partnerId: faker.string.uuid(),
        passenger: null,
        memberId: faker.string.uuid(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: null,
        seat: null,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        bookingCode: null,
        reservationCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data reservation just updated', async () => {
      // arrange
      const id = mockReservation.id;

      const reservationUpdate: IReservationUpdateData = {
        currentStatus: EReservationStatus.PAYMENT_CHARGED,
      };

      const findReservationById = jest
        .spyOn(repository, 'findReservationById')
        .mockResolvedValue(mockReservation);

      mockReservation.currentStatus = reservationUpdate.currentStatus;

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await repository.updateReservation(
        id,
        reservationUpdate,
      );

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findReservationById).toHaveBeenCalledWith(id);
      expect(saveSpy).toHaveBeenCalledWith(mockReservation);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.string.uuid()

      const reservationUpdate: IReservationUpdateData = {
        currentStatus: EReservationStatus.PAYMENT_CHARGED,
      };

      const findReservationById = jest
        .spyOn(repository, 'findReservationById')
        .mockResolvedValue(null);

      const saveSpy = jest.spyOn(repository, 'save').mockResolvedValue(null);

      // act
      const updateReservation = repository.updateReservation(
        id,
        reservationUpdate,
      );

      // assert
      await expect(updateReservation).rejects.toEqual(
        new NotFoundException('Reservation not found'),
      );
      expect(findReservationById).toBeCalledTimes(1);
      expect(findReservationById).toHaveBeenCalledWith(id);
      expect(saveSpy).toBeCalledTimes(0);
    });
  });

  describe('findReservationDetailById', () => {
    let mockReservation: Reservation;
    let mockReservationJourney: ReservationJourney;
    let mockPassenger: Passenger;
    let mockAirport: Airport;
    let mockAirplane: Airplane;
    let mockFlight: Flight;
    let mockSeat: Seat;

    beforeEach(async () => {
      mockReservationJourney = {
        id: faker.string.uuid(),
        description: EReservationStatus.NEW,
        journeyTime: faker.date.recent().toISOString(),
        reservation: mockReservation,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirport = {
        id: faker.string.uuid(),
        icao: faker.airline.airport().iataCode,
        iata: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        airportCategory: EAirportCat.DOMESTIK,
        airportClass: EAirportClass.KELAS_III,
        manageBy: "Unit Penyelenggara Bandar Udara",
        address: "Kab. Kutai Barat, Kalimantan Timur",
        city: fakerID_ID.location.city(),
        timezone: ETimezone.WIB,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockAirplane = {
        id: faker.string.uuid(),
        code: faker.airline.airplane().iataTypeCode,
        registrationNumber: faker.string.alphanumeric(6),
        name: faker.airline.airplane().name,
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        totalSeat: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlight = {
        id: faker.string.uuid(),
        code: faker.airline.flightNumber(),
        airplane: mockAirplane,
        origin: mockAirport,
        destination: mockAirport,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        boardingTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockPassenger = {
        id: faker.string.uuid(),
        identityCategory: EIdentityCat.KTP,
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        email: faker.internet.email(),
        phone: faker.phone.number(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockSeat = {
        id: faker.string.uuid(),
        code: faker.string.alphanumeric(2),
        seatClass: faker.helpers.enumValue(ESeatClass),
        seatSide: faker.helpers.enumValue(ESeatSide),
        seatPosition: faker.helpers.enumValue(ESeatPosition),
        airplane: new Airplane,
        seqRow: faker.number.int({ min: 1 }),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.string.uuid(),
        partnerId: faker.string.uuid(),
        passenger: mockPassenger,
        memberId: faker.string.uuid(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight,
        seat: mockSeat,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [mockReservationJourney],
        bookingCode: null,
        reservationCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return data detail reservation', async () => {
      // arrange
      const id = mockReservation.id;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockReservation);

      // act
      const reservation = await repository.findReservationDetailById(id);

      // assert
      expect(reservation).toEqual(mockReservation);
      expect(findOne).toHaveBeenCalledWith({
        where: { id },
        relations: {
          flight: { origin: true, destination: true },
          passenger: true,
          seat: { airplane: true },
          payments: { details: true },
          journeys: true,
        },
      });
    });

    it('should return null', async () => {
      // arrange
      const id = faker.string.uuid()

      const findOne = jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      // act
      const reservation = await repository.findReservationDetailById(id);

      // assert
      expect(reservation).toEqual(null);
      expect(findOne).toHaveBeenCalledWith({
        where: { id },
        relations: {
          flight: { origin: true, destination: true },
          passenger: true,
          seat: { airplane: true },
          payments: { details: true },
          journeys: true,
        },
      });
    });
  });

  describe('findReservationByCode', () => {
    let mockReservation: Reservation;
    let mockPassenger: Passenger;

    beforeEach(async () => {
      mockPassenger = {
        id: faker.string.uuid(),
        identityCategory: EIdentityCat.KTP,
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date
          .birthdate({ min: 10, max: 56, mode: 'age' })
          .toISOString()
          .split('T')[0],
        email: faker.internet.email(),
        phone: faker.phone.number(),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockReservation = {
        id: faker.string.uuid(),
        partnerId: faker.string.uuid(),
        passenger: mockPassenger,
        memberId: faker.string.uuid(),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: null,
        seat: null,
        priceActual: faker.number.int({ min: 500000 }),
        currentStatus: EReservationStatus.NEW,
        journeys: [],
        bookingCode: null,
        reservationCode: null,
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };
    });

    afterEach(() => jest.clearAllMocks());
    it('should return data from method findReservationByCode', async () => {
      // arrange
      const createQueryBuilder: any = {
        innerJoinAndSelect: () => createQueryBuilder,
        where: () => createQueryBuilder,
        andWhere: () => createQueryBuilder,
        getOne: jest.fn().mockReturnValue(mockReservation)
      }
      jest.spyOn(repository, 'createQueryBuilder').mockImplementation(() => createQueryBuilder)

      // act
      const getData = await repository.findReservationByCode(mockPassenger.identityNumber, mockReservation.reservationCode);

      // assert
      expect(getData).toEqual(mockReservation);
    });
  })
});
