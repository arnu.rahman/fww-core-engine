import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { Airplane } from '../entities/airplane.entity';
import { AirplaneRepository } from '../repository/airplane.repository';

describe('AirplaneRepository', () => {
  let repository: AirplaneRepository;
  let mockAirplane: Airplane;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AirplaneRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<AirplaneRepository>(AirplaneRepository);

    mockAirplane = {
      id: faker.string.uuid(),
      code: faker.airline.airplane().iataTypeCode,
      registrationNumber: faker.string.alphanumeric(6),
      name: faker.airline.airplane().name,
      maxBusiness: faker.number.int({ min: 0 }),
      maxEconomy: faker.number.int({ min: 50 }),
      totalSeat: faker.number.int({ min: 1 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });
});
