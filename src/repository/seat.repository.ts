import { Injectable, Logger } from '@nestjs/common';
import { Seat } from '../entities/seat.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class SeatRepository extends Repository<Seat> {
  private readonly logger = new Logger(SeatRepository.name);
  constructor(dataSource: DataSource) {
    super(Seat, dataSource.createEntityManager());
  }

  async findOneBySeatId(id: string): Promise<Seat> {
    const seat = await this.findOneBy({ id });
    this.logger.log('Query data seat by id');
    return seat;
  }
}
