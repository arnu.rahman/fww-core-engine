import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { IReservationNew } from '../interface/reservation-new.interface';
import { IReservationUpdateData } from '../interface/reservation-update.interface';
import { Reservation } from '../entities/reservation.entity';

@Injectable()
export class ReservationRepository extends Repository<Reservation> {
  private readonly logger = new Logger(ReservationRepository.name);
  constructor(dataSource: DataSource) {
    super(Reservation, dataSource.createEntityManager());
  }

  async findReservationById(id: string): Promise<Reservation> {
    const reservation = await this.findOneBy({ id });
    this.logger.log('Query data reservation');
    return reservation;
  }

  async saveReservation(reservation: IReservationNew): Promise<Reservation> {
    const newReservation = await this.save(reservation);
    this.logger.log('Insert new reservation');
    return newReservation;
  }

  async updateReservation(
    id: string,
    reservationUpdate: IReservationUpdateData,
  ): Promise<Reservation> {
    let reservation = await this.findReservationById(id);
    if (!reservation) {
      throw new NotFoundException('Reservation not found');
    }

    reservation = { ...reservation, ...reservationUpdate };

    await this.save(reservation);
    this.logger.log('Update data reservation');
    return reservation;
  }

  async findReservationDetailById(id: string): Promise<Reservation> {
    const reservation = await this.findOne({
      where: { id },
      relations: {
        flight: { origin: true, destination: true },
        passenger: true,
        seat: { airplane: true },
        payments: { details: true },
        journeys: true,
      },
    });
    this.logger.log('Query data reservation');
    return reservation;
  }

  async findReservationByCode(identityNumber: string, reservationCode: string) {
    const reservationQB = this.createQueryBuilder('reservation')
      .innerJoinAndSelect('reservation.passenger', 'passenger')
      .where('reservation.reservationCode = :code', {
        code: reservationCode,
      })
      .andWhere('passenger.identityNumber = :identityNumber', {
        identityNumber: identityNumber,
      });

    const reservation = await reservationQB.getOne();
    this.logger.log('Query data reservation');
    return reservation;
  }
}
