import { Injectable, Logger } from '@nestjs/common';
import {
  DataSource,
  Repository,
} from 'typeorm';

import { FilterFlightDto } from '../dto/core/filter-flight.dto';
import { SeatClassDto } from '../dto/core/seat-class.dto';
import { FlightSeatDto } from '../dto/core/flight-seat.dto';
import { FlightCandidateDto } from '../dto/core/flight-candidate.dto';
import { Flight } from '../entities/flight.entity';

@Injectable()
export class FlightRepository extends Repository<Flight> {
  private readonly logger = new Logger(FlightRepository.name);
  constructor(dataSource: DataSource) {
    super(Flight, dataSource.createEntityManager());
  }

  async findByIdIncludeBaggages(id: string): Promise<Flight> {
    const flight = await this.findOne({
      relations: ['baggage'],
      where: { id },
    });
    this.logger.log('Query data flight include baggages with criteria');
    return flight;
  }

  async findByIdIncludeAirports(id: string): Promise<Flight> {
    const flight = await this.findOne({
      relations: ['origin', 'destination', 'airplane'],
      where: { id },
    });
    this.logger.log('Query data flight include airports with criteria');
    return flight;
  }

  async findByIdIncludeSeats(id: string, query: SeatClassDto): Promise<Flight> {
    const flightQB = this.createQueryBuilder('flight')
      .innerJoinAndSelect('flight.airplane', 'airplane')
      .innerJoinAndSelect('airplane.seats', 'seat')
      .where('flight.id = :id', { id });

    if (query.seatClass) {
      flightQB.andWhere('seat.seatClass = :seatClass', {
        seatClass: query.seatClass,
      });
    }

    const flight = await flightQB.getOne();
    this.logger.log('Query data flight include seats with criteria');
    return flight;
  }

  async findByIdIncludeSpecificSeat(
    flightSeatDto: FlightSeatDto,
  ): Promise<Flight> {
    const flightQB = this.createQueryBuilder('flight')
      .innerJoinAndSelect('flight.airplane', 'airplane')
      .innerJoinAndSelect('airplane.seats', 'seat')
      .where('flight.id = :flightId', { flightId: flightSeatDto.flight })
      .andWhere('seat.id = :seatId', {
        seatId: flightSeatDto.seat,
      });
    const flight = await flightQB.getOne();

    this.logger.log('Query data flight include specific seat');
    return flight;
  }

  async findWithQuery(query: FilterFlightDto): Promise<Flight[]> {
    const flightQB = this.createQueryBuilder('flight')
      .innerJoinAndSelect('flight.origin', 'origin')
      .innerJoinAndSelect('flight.destination', 'destination')
      .innerJoinAndSelect('flight.airplane', 'airplane')
      .innerJoinAndSelect('flight.prices', 'price')
      .innerJoinAndSelect('flight.baggage', 'baggage');

    if (query.origin) {
      flightQB.andWhere(`origin.icao LIKE "%${query.origin}%" or
      origin.iata LIKE "%${query.origin}%" or
      origin.name LIKE "%${query.origin}%" or
      origin.city LIKE "%${query.origin}%"`)
    }

    if (query.destination) {
      flightQB.andWhere(`destination.icao LIKE "%${query.destination}%" or
      destination.iata LIKE "%${query.destination}%" or
      destination.name LIKE "%${query.destination}%" or
      destination.city LIKE "%${query.destination}%"`)
    }
    const flights = await flightQB.getMany();
    this.logger.log('Query data flights with criteria');
    return flights;
  }

  async findCandidate(criteria: FlightCandidateDto): Promise<Flight[]> {
    const flightQB = this.createQueryBuilder('flight')
      .innerJoinAndSelect('flight.origin', 'origin')
      .innerJoinAndSelect('flight.destination', 'destination')
      .innerJoinAndSelect('flight.airplane', 'airplane')
      .innerJoinAndSelect('flight.prices', 'price')
      .innerJoinAndSelect('flight.baggage', 'baggage')
      .where('flight.origin = :origin', {
        origin: criteria.originAirportId,
      })
      .andWhere('flight.destination = :destination', {
        destination: criteria.destinationAirportId,
      });

    const flights = await flightQB.getMany();

    this.logger.log('Query data flight candidates with criteria');
    return flights;
  }
}
