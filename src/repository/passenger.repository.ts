import { Injectable, Logger } from '@nestjs/common';
import { Passenger } from '../entities/passenger.entity';
import { DataSource, Repository } from 'typeorm';
import { IPassenger } from '../interface/passenger.interface';

@Injectable()
export class PassengerRepository extends Repository<Passenger> {
  private readonly logger = new Logger(PassengerRepository.name);
  constructor(dataSource: DataSource) {
    super(Passenger, dataSource.createEntityManager());
  }

  async getOne(identityNumber: string): Promise<Passenger> {
    const passenger = await this.findOne({ where: { identityNumber } });
    this.logger.log('Query data passenger');
    return passenger;
  }

  async savePassenger(passenger: IPassenger): Promise<Passenger> {
    const passengerExist = await this.getOne(passenger.identityNumber);
    if (!passengerExist) {
      const newPassenger = await this.save(passenger);
      this.logger.log('Insert new passenger');
      return newPassenger;
    }

    passengerExist.name = passenger.name;
    passengerExist.birthDate = passenger.birthDate;
    const updatedPassenger = await this.save(passengerExist);
    this.logger.log('Update existing passenger');
    return updatedPassenger;
  }
}
