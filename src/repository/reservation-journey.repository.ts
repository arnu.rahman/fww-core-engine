import { Injectable, Logger } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { IReservationJourney } from '../interface/reservation-new.interface';
import { ReservationJourney } from '../entities/reservation-journey.entity';

@Injectable()
export class ReservationJourneyRepository extends Repository<ReservationJourney> {
  private readonly logger = new Logger(ReservationJourneyRepository.name);
  constructor(dataSource: DataSource) {
    super(ReservationJourney, dataSource.createEntityManager());
  }

  async write(
    reservationJourney: IReservationJourney,
  ): Promise<ReservationJourney> {
    const newReservationJourney = await this.save(reservationJourney);
    this.logger.log('Insert new reservation journey');
    return newReservationJourney;
  }
}
