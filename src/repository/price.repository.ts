import { Injectable, Logger } from '@nestjs/common';
import { Price } from '../entities/price.entity';
import { IPriceSearch } from '../interface/price-search.interface';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class PriceRepository extends Repository<Price> {
  private readonly logger = new Logger(PriceRepository.name);
  constructor(dataSource: DataSource) {
    super(Price, dataSource.createEntityManager());
  }

  async findByFlightAndSeatClass(criteria: IPriceSearch): Promise<Price> {
    const flightQB = this.createQueryBuilder('price')
      .where('price.flight = :flight', {
        flight: criteria.flight,
      })
      .andWhere('price.seat_class = :seatClass', {
        seatClass: criteria.seatClass,
      });

    const price = await flightQB.getOne();

    this.logger.log('Query data price with criteria');
    return price;
  }
}
