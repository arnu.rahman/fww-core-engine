import { Injectable, Logger } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { Airplane } from '../entities/airplane.entity';

@Injectable()
export class AirplaneRepository extends Repository<Airplane> {
  private readonly logger = new Logger(AirplaneRepository.name);
  constructor(dataSource: DataSource) {
    super(Airplane, dataSource.createEntityManager());
  }
}
