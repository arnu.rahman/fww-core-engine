import { Injectable, Logger } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { Baggage } from '../entities/baggage.entity';

@Injectable()
export class BaggageRepository extends Repository<Baggage> {
  private readonly logger = new Logger(BaggageRepository.name);
  constructor(dataSource: DataSource) {
    super(Baggage, dataSource.createEntityManager());
  }
}
