import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { Payment } from '../entities/payment.entity';
import {
  IPaymentMaster,
  IPaymentUpdate,
} from '../interface/payment.interface';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class PaymentRepository extends Repository<Payment> {
  private readonly logger = new Logger(PaymentRepository.name);
  constructor(dataSource: DataSource) {
    super(Payment, dataSource.createEntityManager());
  }

  async writeNew(dataPayment: IPaymentMaster): Promise<Payment> {
    const newPayment = await this.save(dataPayment);
    this.logger.log('Insert new payment');
    return newPayment;
  }

  async updateStatus(dataPayment: IPaymentUpdate): Promise<Payment> {
    let payment = await this.findOne({
      where: { id: dataPayment.id },
    });

    if (!payment) {
      throw new NotFoundException('Payment not found');
    }

    payment = { ...payment, ...dataPayment };
    await this.save(payment);

    this.logger.log('Update data payment');
    return payment;
  }
}
