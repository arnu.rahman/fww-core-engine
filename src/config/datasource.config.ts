import { DataSource, DataSourceOptions } from "typeorm";
import * as dotenv from 'dotenv'
import { Airplane } from "../entities/airplane.entity";
import { Airport } from "../entities/airport.entity";
import { Baggage } from "../entities/baggage.entity";
import { FlightBaggage } from "../entities/flight-baggage.entity";
import { Flight } from "../entities/flight.entity";
import { Passenger } from "../entities/passenger.entity";
import { PaymentDetail } from "../entities/payment-detail.entity";
import { Payment } from "../entities/payment.entity";
import { Price } from "../entities/price.entity";
import { Reservation } from "../entities/reservation.entity";
import { ReservationJourney } from "../entities/reservation-journey.entity";
import { Seat } from "../entities/seat.entity";
dotenv.config();

export const dataSourceOptions: DataSourceOptions = {
    type: 'mariadb',
    host: process.env.MYSQL_HOST,
    port: parseInt(process.env.MYSQL_PORT),
    username: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database:  process.env.MYSQL_DATABASE,
    entities: [Airplane, Airport, Baggage, FlightBaggage, Flight, Passenger, PaymentDetail, Payment, Price, Reservation, ReservationJourney, Seat],
    synchronize: false,
    logging: false,
    dateStrings: true
}

const dataSource = new DataSource(dataSourceOptions);
dataSource.initialize();
export default dataSource;
