import { EBankChoice } from '../helpers/enum/bank-choice.enum';
import { IGenericId } from './generic-id.interface';
import { EPaymentMethod } from '../helpers/enum/payment-method.enum';

export interface IPaymentMaster {
  reservation?: IGenericId;
  paymentMethod: EPaymentMethod;
  bankChoice: EBankChoice;
  paymentFinal: number;
  paymentStatus: string;
  chargeTime: string;
  paymentTime?: string;
  details?: IPaymentDetail[];
}

export interface IPaymentDetail {
  id: string;
  payment?: IPaymentMaster;
  name: string;
  quantity: number;
  price: number;
}

export interface IPaymentUpdate {
  id: string;
  paymentStatus: string;
  checkTime: string;
  paymentTime?: string;
}
