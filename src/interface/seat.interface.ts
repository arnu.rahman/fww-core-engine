import { ESeatClass } from "../helpers/enum/seat-class.enum";
import { ESeatPosition } from "../helpers/enum/seat-position.enum";
import { ESeatSide } from "../helpers/enum/seat-side.enum";

export interface ISeat {
  id: string;
  code: string;
  seatClass: ESeatClass;
  side: ESeatSide;
  position: ESeatPosition;
}
