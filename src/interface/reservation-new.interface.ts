import { EReservationStatus } from '../helpers/enum/reservation-status.enum';
import { IGenericId } from './generic-id.interface';
import { IPassenger } from './passenger.interface';

export interface IReservationNew {
  partnerId: string;
  memberId: string;
  passenger: IPassenger;
  flightDate: string;
  flight: IGenericId;
  seat: IGenericId;
  priceActual: number;
  currentStatus: EReservationStatus;
  journeys: IReservationJourney[];
}

export interface IReservationJourney {
  reservation?: IGenericId;
  description: EReservationStatus;
  journeyTime: string;
}
