import { ESeatClass } from "../helpers/enum/seat-class.enum";

export interface IPriceSearch {
  flight: string;
  seatClass: ESeatClass;
}
