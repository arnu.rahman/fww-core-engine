import { Airport } from 'src/entities/airport.entity';
import { IAirplaneWithSpecificSeat } from './airplane-with-specific-seat.interface';

export interface IFlightWithSpecificSeat {
  id: string;
  code: string;
  origin: Airport;
  destination: Airport;
  airplane: IAirplaneWithSpecificSeat;
  departureTimeInWIB: string;
  arrivalTimeInWIB: string;
  durationInMinutes: number;
}
