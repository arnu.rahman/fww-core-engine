import { ISeat } from './seat.interface';

export interface IAirplaneWithSpecificSeat {
  id: string;
  name: string;
  registrationNumber: string;
  maxBusiness: number;
  maxEconomy: number;
  totalSeat: number;
  seat: ISeat;
}
