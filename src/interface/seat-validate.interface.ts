import { Airport } from "src/entities/airport.entity";
import { ESeatClass } from "../helpers/enum/seat-class.enum";

export interface ISeatValidate {
  isValid: boolean;
  flight: string;
  origin: Airport;
  destination: Airport;
  airplane: string;
  seat: string;
  seatClass: ESeatClass;
  seatNumber: string;
  price: number;
}
