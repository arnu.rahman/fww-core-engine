import { EIdentityCat } from "../helpers/enum/identity-category.enum";

export interface IPassenger {
  identityCategory: EIdentityCat;
  identityNumber: string;
  name?: string;
  birthDate?: string;
  email?: string;
  phone?: string;
}
