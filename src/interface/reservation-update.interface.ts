import { EReservationStatus } from "../helpers/enum/reservation-status.enum";

export interface IReservationUpdateRequest {
  id: string;
  status: EReservationStatus;
  journeyTime: string;
  bookingCode?: string;
  reservationCode?: string;
  ticketNumber?: string;
}

export interface IReservationUpdateData {
  currentStatus: EReservationStatus;
  bookingCode?: string;
  reservationCode?: string;
}
