export enum ETimezone {
  WIB = 'Asia/Jakarta',
  WITA = 'Asia/Makassar',
  WIT = 'Asia/Jayapura',
}
