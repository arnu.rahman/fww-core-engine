export enum EPaymentMethod {
  QRIS = 'QRIS',
  TRANSFER = 'TRANSFER',
}
