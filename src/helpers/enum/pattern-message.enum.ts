export enum EventPatternMessage {
  UPDATE_RESERVATION = 'EPM_UpdateReservation',
  CHARGE_PAYMENT = 'EPM_ChargePayment',
  UPDATE_PAYMENT = 'EPM_UpdatePayment',
}
