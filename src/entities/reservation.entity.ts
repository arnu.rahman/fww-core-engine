import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Flight } from './flight.entity';
import { Seat } from './seat.entity';
import { Passenger } from './passenger.entity';
import { Payment } from './payment.entity';
import { ReservationJourney } from './reservation-journey.entity';
import { EReservationStatus } from '../helpers/enum/reservation-status.enum';

@Entity('reservation')
export class Reservation {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Index()
  @Column({ name: 'booking_code', length: 36, nullable: true })
  bookingCode: string;

  @Index()
  @Column({ name: 'reservation_code', length: 36, nullable: true })
  reservationCode: string;

  @Index()
  @Column({ name: 'partner_id', type: 'uuid', nullable: true })
  partnerId: string;

  @Column({ name: 'member_id', type: 'uuid', nullable: true })
  memberId: string;

  @ManyToOne(() => Passenger, (passanger) => passanger.identityNumber)
  @JoinColumn({ name: 'passenger_id' })
  @Column({ name: 'passenger_id', type: 'uuid'})
  passenger?: Passenger;

  @ManyToOne(() => Flight, (flight) => flight.id)
  @JoinColumn({ name: 'flight_id' })
  @Column({ name: 'flight_id', type: 'uuid'})
  flight?: Flight;

  @Index()
  @Column({ name: 'flight_date', type: 'date' })
  flightDate: string;

  @ManyToOne(() => Seat, (seat) => seat.id)
  @JoinColumn({ name: 'seat_id' })
  @Column({ name: 'seat_id', type: 'uuid'})
  seat?: Seat;

  @Column({ name: 'price_actual', type: 'double' })
  priceActual: number;

  @Index()
  @Column({ name: 'current_status', length: 20 })
  currentStatus: EReservationStatus;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;

  @OneToMany(() => Payment, (payment) => payment.reservation)
  payments?: Payment[];

  @OneToMany(() => ReservationJourney, (journey) => journey.reservation)
  journeys?: ReservationJourney[];
}
