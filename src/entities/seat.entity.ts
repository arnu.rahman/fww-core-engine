import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Airplane } from './airplane.entity';
import { ESeatClass } from '../helpers/enum/seat-class.enum';
import { ESeatSide } from '../helpers/enum/seat-side.enum';
import { ESeatPosition } from '../helpers/enum/seat-position.enum';

@Entity('airplane_seat')
export class Seat {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Index()
  @Column({ length: 10 })
  code: string;

  @Column({ name: 'seat_class'})
  seatClass: ESeatClass;

  @Column({ name: 'seat_side' })
  seatSide: ESeatSide;

  @Column({ name: 'seat_position' })
  seatPosition: ESeatPosition;

  @ManyToOne(() => Airplane, (airplane) => airplane.id)
  @JoinColumn({ name: 'airplane_id' })
  @Column({ name: 'airplane_id', type: 'uuid'})
  airplane?: Airplane;

  @Column({ name: 'seq_row'})
  seqRow: number;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
