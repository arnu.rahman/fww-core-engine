import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Airport } from './airport.entity';
import { Airplane } from './airplane.entity';
import { Price } from './price.entity';
import { Baggage } from './baggage.entity';

@Entity('flight')
export class Flight {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 10 })
  code: string;

  @ManyToOne(() => Airplane, (airplane) => airplane.id)
  @JoinColumn({ name: 'airplane_id' })
  @Column({ name: 'airplane_id', type: 'uuid'})
  airplane?: Airplane;

  @ManyToOne(() => Airport, (origin) => origin.id)
  @JoinColumn({ name: 'origin_airport_id' })
  @Column({ name: 'origin_airport_id', type: 'uuid'})
  origin?: Airport;

  @ManyToOne(() => Airport, (destination) => destination.id)
  @JoinColumn({ name: 'destination_airport_id' })
  @Column({ name: 'destination_airport_id', type: 'uuid'})
  destination?: Airport;

  @Column({ name: 'departure_time', type: 'time' })
  departureTime: string;

  @Column({ name: 'arrival_time', type: 'time' })
  arrivalTime: string;

  @Column({ name: 'boarding_time', type: 'time' })
  boardingTime: string;

  @Column({ name: 'duration', type: 'int' })
  durationInMinutes: number;

  @OneToMany(() => Baggage, (baggage) => baggage.flight)
  baggage?: Baggage[];

  @OneToMany(() => Price, (price) => price.flight)
  prices?: Price[];

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
