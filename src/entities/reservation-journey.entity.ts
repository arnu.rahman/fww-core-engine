import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Reservation } from './reservation.entity';
import { EReservationStatus } from '../helpers/enum/reservation-status.enum';

@Entity('reservation_journey')
export class ReservationJourney {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Reservation, (reservation) => reservation.id)
  @JoinColumn({ name: 'reservation_id' })
  @Column({ name: 'reservation_id', type: 'uuid'})
  reservation?: Reservation;

  @Column({ length: 40 })
  description: EReservationStatus;

  @Column({ name: 'journey_time', type: 'datetime' })
  journeyTime: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
