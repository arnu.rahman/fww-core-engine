import { EIdentityCat } from '../helpers/enum/identity-category.enum';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('passenger')
export class Passenger {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'identity_category' })
  identityCategory: EIdentityCat;

  @Index()
  @Column({ name: 'identity_number', length: 20 })
  identityNumber: string;

  @Column({ length: 75 })
  name: string;

  @Column({ name: 'birth_date', type: 'date' })
  birthDate: string;

  @Index()
  @Column({ length: 45 })
  email: string;

  @Column({ length: 45 })
  phone: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
