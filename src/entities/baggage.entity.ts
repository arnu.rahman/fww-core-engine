import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Flight } from './flight.entity';

@Entity('baggage')
export class Baggage {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Flight, (flight) => flight.id)
  @JoinColumn({ name: 'flight_id' })
  @Column({ name: 'flight_id', type: 'uuid'})
  flight?: Flight;

  @Column({ type: 'smallint' })
  capacity: number;

  @Column({ length: 125 })
  category: string;

  @Column({ name: 'charge_price', type: 'int' })
  chargePrice: number;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
