import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Reservation } from './reservation.entity';
import { PaymentDetail } from './payment-detail.entity';
import { EBankChoice } from '../helpers/enum/bank-choice.enum';
import { EPaymentMethod } from '../helpers/enum/payment-method.enum';

@Entity('payment')
export class Payment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Reservation, (reservation) => reservation.id)
  @JoinColumn({ name: 'reservation_id' })
  @Column({ name: 'reservation_id', type: 'uuid'})
  reservation?: Reservation;

  @Column({ name: 'method'})
  paymentMethod: EPaymentMethod;

  @Column({ name: 'bank_choice'})
  bankChoice: EBankChoice;

  @Column({ name: 'payment_final', type: 'double' })
  paymentFinal: number;

  @Column({ name: 'payment_status' })
  paymentStatus: string;

  @Column({ name: 'charge_time', type: 'datetime' })
  chargeTime: string;

  @Column({ name: 'last_payment_time', type: 'datetime', nullable: true })
  checkTime: string;

  @Column({ name: 'payment_time', type: 'datetime', nullable: true })
  paymentTime: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;

  @OneToMany(() => PaymentDetail, (paymentDetail) => paymentDetail.payment)
  details?: PaymentDetail[];
}
