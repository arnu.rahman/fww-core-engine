import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Seat } from './seat.entity';

@Entity('airplane')
export class Airplane {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 10 })
  code: string;

  @Column({ name: 'registration_number', length: 30 })
  registrationNumber: string;

  @Column({ length: 50 })
  name: string;

  @Column({ name: 'max_business'})
  maxBusiness: number;

  @Column({ name: 'max_economy'})
  maxEconomy: number;

  @Column({ name: 'total_seat'})
  totalSeat: number;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;

  @OneToMany(() => Seat, (seat) => seat.airplane)
  seats?: Seat[];
}
