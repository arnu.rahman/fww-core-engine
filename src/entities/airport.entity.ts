import { EAirportCat } from '../helpers/enum/airport-category.enum';
import { EAirportClass } from '../helpers/enum/airport-class.enum';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('airport')
export class Airport {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Index()
  @Column({ length: 4, unique: true })
  icao: string;

  @Index()
  @Column({ length: 3, unique: true })
  iata: string;

  @Index()
  @Column({ length: 125 })
  name: string;

  @Column({ name: 'category' })
  airportCategory: EAirportCat;

  @Column({ name: 'class' })
  airportClass: EAirportClass;

  @Column({ name: 'manage_by', length: 255 })
  manageBy: string;

  @Column({ length: 255 })
  address: string;

  @Index()
  @Column({ length: 75 })
  city: string;

  @Column({ length: 25 })
  timezone: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
