import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Baggage } from './baggage.entity';
import { Flight } from './flight.entity';

@Entity('flight_baggage')
export class FlightBaggage {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Baggage, (baggage) => baggage.id)
  @JoinColumn({ name: 'baggage_id' })
  @Column({ type: 'uuid'})
  baggages?: Baggage;

  @ManyToOne(() => Flight, (flight) => flight.id)
  @JoinColumn({ name: 'flight_id' })
  @Column({ type: 'uuid'})
  flights?: Flight;
}
