import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Flight } from './flight.entity';
import { ESeatClass } from '../helpers/enum/seat-class.enum';

@Entity('price')
export class Price {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Flight, (flight) => flight.id)
  @JoinColumn({ name: 'flight_id' })
  @Column({ name: 'flight_id', type: 'uuid'})
  flight?: Flight;

  @Column({ name: 'seat_class' })
  seatClass: ESeatClass;

  @Column({ name: 'price', type: 'double' })
  price: number;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
