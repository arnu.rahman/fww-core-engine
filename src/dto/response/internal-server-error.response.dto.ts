import { ApiProperty } from '@nestjs/swagger';

export class InternalServerErrorResponseDto{
  @ApiProperty({ example: 500 })
  statusCode: number;

  @ApiProperty({ example: 'This is sample message internal server error' })
  message: string;

  @ApiProperty({ example: 'Forbidden' })
  error: string;
}
