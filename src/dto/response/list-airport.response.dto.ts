import { ApiProperty } from '@nestjs/swagger';
import { AirportDto } from '../core/airport.dto';

export class ListAirportResponseDto{
  @ApiProperty({ example: 200 })
  statusCode: number;

  @ApiProperty({
    example: 'This is sample message get list data successfully',
  })
  message: string;

  @ApiProperty({ type: AirportDto, isArray: true })
  data: AirportDto[];
}
