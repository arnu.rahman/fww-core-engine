import { ApiProperty } from '@nestjs/swagger';
import { FlightDto } from '../core/flight.dto';

export class ListFlightResponseDto{
  @ApiProperty({ example: 200 })
  statusCode: number;

  @ApiProperty({
    example: 'This is sample message get list data successfully',
  })
  message: string;

  @ApiProperty({ type: FlightDto, isArray: true })
  data: FlightDto[];
}
