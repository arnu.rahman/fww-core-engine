import { OmitType } from "@nestjs/swagger";
import { AirplaneDto } from "../core/airplane.dto";

export class CreateAirplaneDto extends OmitType(AirplaneDto,['id']){}
