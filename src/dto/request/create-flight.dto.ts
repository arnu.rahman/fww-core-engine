import { OmitType } from "@nestjs/swagger";
import { FlightDto } from "../core/flight.dto";

export class CreateFlightDto extends OmitType(FlightDto,['id']){}
