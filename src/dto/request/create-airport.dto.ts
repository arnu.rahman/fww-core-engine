import { OmitType } from "@nestjs/swagger";
import { AirportDto } from "../core/airport.dto";

export class CreateAirportDto extends OmitType(AirportDto,['id']){}
