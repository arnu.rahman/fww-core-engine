import { OmitType } from "@nestjs/swagger";
import { BaggageDto } from "../core/baggage.dto";

export class CreateBaggageDto extends OmitType(BaggageDto,['id']){}
