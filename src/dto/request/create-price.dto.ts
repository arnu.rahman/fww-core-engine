import { OmitType } from "@nestjs/swagger";
import { PriceDto } from "../core/price.dto";

export class CreatePriceDto extends OmitType(PriceDto,['id']){}
