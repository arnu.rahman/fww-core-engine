import { OmitType } from "@nestjs/swagger";
import { SeatDto } from "../core/seat.dto";

export class CreateSeatDto extends OmitType(SeatDto,['id']){}
