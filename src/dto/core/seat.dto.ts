import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ESeatClass } from '../../helpers/enum/seat-class.enum';
import { ESeatPosition } from '../../helpers/enum/seat-position.enum';
import { ESeatSide } from '../../helpers/enum/seat-side.enum';
import { AirplaneDto } from './airplane.dto';

export class SeatDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  airplane: AirplaneDto;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  code: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(ESeatClass)
  seatClass: ESeatClass;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(ESeatSide)
  seatSide: ESeatSide;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(ESeatPosition)
  seatPosition: ESeatPosition;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  seqRow: number;
}
