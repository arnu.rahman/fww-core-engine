import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class FlightCandidateDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  originAirportId: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  destinationAirportId: string;
}
