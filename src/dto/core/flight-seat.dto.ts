import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class FlightSeatDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  flight: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  seat: string;
}
