import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUUID,
  Length,
} from 'class-validator';
import { EIdentityCat } from '../../helpers/enum/identity-category.enum';

export class ReservationDto {
  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  partnerId: string;

  @ApiProperty()
  @IsOptional()
  @IsUUID()
  memberId: string;

  @ApiProperty()
  @IsEnum(EIdentityCat)
  @IsNotEmpty()
  identityCategory: EIdentityCat;

  @ApiProperty()
  @IsString()
  @Length(10)
  @IsNotEmpty()
  identityNumber: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsDateString()
  @IsNotEmpty()
  birthDate: string;

  @ApiProperty()
  @IsPhoneNumber()
  @IsNotEmpty()
  phone: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsDateString()
  @IsNotEmpty()
  flightDate: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  flightId: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  seatId: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  priceActual: number;

  @ApiProperty()
  @IsDateString()
  @IsNotEmpty()
  reservationTime: string;
}
