import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ESeatClass } from '../../helpers/enum/seat-class.enum';
import { FlightDto } from './flight.dto';

export class PriceDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  flight: FlightDto;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(ESeatClass)
  seatClass: ESeatClass;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  price: number;
}
