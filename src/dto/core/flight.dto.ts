import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { AirportDto } from './airport.dto';
import { AirplaneDto } from './airplane.dto';

export class FlightDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  code: string;

  @ApiProperty()
  @IsNotEmpty()
  airplane: AirplaneDto;

  @ApiProperty()
  @IsNotEmpty()
  origin: AirportDto;

  @ApiProperty()
  @IsNotEmpty()
  destination: AirportDto;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  departureTime: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  arrivalTime: string;

  @ApiProperty()
  @IsString()
  boardingTime: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  durationInMinutes: number;
}
