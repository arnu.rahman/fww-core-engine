import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { EAirportCat } from '../../helpers/enum/airport-category.enum';
import { EAirportClass } from '../../helpers/enum/airport-class.enum';
import { ETimezone } from '../../helpers/enum/timezone.enum';

export class AirportDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  id: string;

  @ApiProperty()
  @IsString()
  icao: string;

  @ApiProperty()
  @IsString()
  iata: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(EAirportCat)
  airportCategory: EAirportCat;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(EAirportClass)
  airportClass: EAirportClass;

  @ApiProperty()
  @IsString()
  manageBy: string;

  @ApiProperty()
  @IsString()
  address: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  city: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(ETimezone)
  timezone: ETimezone;
}
