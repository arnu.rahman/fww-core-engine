import { IsOptional, IsString } from 'class-validator';

export class FilterFlightDto {
  @IsString()
  @IsOptional()
  origin: string;

  @IsString()
  @IsOptional()
  destination: string;
}
