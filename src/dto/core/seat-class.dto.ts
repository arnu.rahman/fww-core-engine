import { IsEnum, IsOptional } from 'class-validator';
import { ESeatClass } from '../../helpers/enum/seat-class.enum';

export class SeatClassDto {
  @IsEnum(ESeatClass)
  @IsOptional()
  seatClass: ESeatClass;
}
