import { Controller, Logger, Get, Post, Body, Patch, Param, Delete, UseGuards, ForbiddenException } from '@nestjs/common';
import { PriceService } from '../services/price.service';
import { CreatePriceDto } from '../dto/request/create-price.dto';
import { UpdatePriceDto } from '../dto/request/update-price.dto';
import { ApiBasicAuth, ApiForbiddenResponse, ApiTags } from '@nestjs/swagger';
import { InternalServerErrorResponseDto } from '../dto/response/internal-server-error.response.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Price')
@ApiForbiddenResponse({ type: InternalServerErrorResponseDto })
@ApiBasicAuth()
@Controller({ path: 'price', version: '1' })
export class PriceController {
  private readonly logger = new Logger(PriceController.name);
  constructor(private readonly priceService: PriceService) { }

  @UseGuards(AuthGuard('basic'))
  @Post()
  async create(@Body() createPriceDto: CreatePriceDto) {
    this.logger.log('[POST] /api/v1/price');
    try {
      const result = await this.priceService.create(createPriceDto);
      this.logger.log(
        `Return result of create price data ${JSON.stringify(createPriceDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get()
  async findAll() {
    this.logger.log('[GET] /api/v1/price');
    try {
      const prices = await this.priceService.findAll();
      this.logger.log(
        `Return all data price with id`,
      );
      return prices;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get(':id')
  async findOne(@Param('id') id: string) {
    this.logger.log('[GET] /api/v1/price');
    try {
      const prices = await this.priceService.findOne(id);
      this.logger.log(
        `Return data price with id ${JSON.stringify(id)}`,
      );
      return prices;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updatePriceDto: UpdatePriceDto) {
    this.logger.log('[PATCH] /api/v1/price');
    try {
      const result = await this.priceService.update(id,updatePriceDto);
      this.logger.log(
        `Return result of update price data ${JSON.stringify(updatePriceDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Delete(':id')
  async remove(@Param('id') id: string) {
    this.logger.log('[DELETE] /api/v1/price');
    try {
      const result = await this.priceService.remove(id);
      this.logger.log(
        `Return result of delete price data with id ${id}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
}
