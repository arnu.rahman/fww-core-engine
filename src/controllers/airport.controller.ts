import { Controller, Logger, Get, Post, Body, Patch, Param, Delete ,UseGuards, ForbiddenException } from '@nestjs/common';
import { AirportService } from '../services/airport.service';
import { CreateAirportDto } from '../dto/request/create-airport.dto';
import { UpdateAirportDto } from '../dto/request/update-airport.dto';
import { ApiBasicAuth, ApiForbiddenResponse, ApiTags  } from '@nestjs/swagger';
import { InternalServerErrorResponseDto } from '../dto/response/internal-server-error.response.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Airport')
@ApiForbiddenResponse({ type: InternalServerErrorResponseDto })
@ApiBasicAuth()
@Controller({ path: 'airport', version: '1' })
export class AirportController {
  private readonly logger = new Logger(AirportController.name);
  constructor(private readonly airportService: AirportService) { }

  @UseGuards(AuthGuard('basic'))
  @Post()
  async create(@Body() createAirportDto: CreateAirportDto) {
    this.logger.log('[POST] /api/v1/airport');
    try {
      const result = await this.airportService.create(createAirportDto);
      this.logger.log(
        `Return result of create airport data ${JSON.stringify(createAirportDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
  @UseGuards(AuthGuard('basic'))
  @Get()
  async findAll() {
    this.logger.log('[GET] /api/v1/airport');
    try {
      const airports = await this.airportService.findAll();
      this.logger.log(
        `Return all data airport with id`,
      );
      return airports;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get(':id')
  async findOne(@Param('id') id: string) {
    this.logger.log('[GET] /api/v1/airport');
    try {
      const airports = await this.airportService.findOne(id);
      this.logger.log(
        `Return data airport with id ${JSON.stringify(id)}`,
      );
      return airports;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateAirportDto: UpdateAirportDto) {
    this.logger.log('[PATCH] /api/v1/airport');
    try {
      const result = await this.airportService.update(id,updateAirportDto);
      this.logger.log(
        `Return result of update airport data ${JSON.stringify(updateAirportDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Delete(':id')
  async remove(@Param('id') id: string) {
    this.logger.log('[DELETE] /api/v1/airport');
    try {
      const result = await this.airportService.remove(id);
      this.logger.log(
        `Return result of delete airport data with id ${id}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
}
