import { Controller, Logger, Get, Post, Body, Patch, Param, Delete, UseGuards, ForbiddenException } from '@nestjs/common';
import { SeatService } from '../services/seat.service';
import { CreateSeatDto } from '../dto/request/create-seat.dto';
import { UpdateSeatDto } from '../dto/request/update-seat.dto';
import { ApiBasicAuth, ApiForbiddenResponse, ApiTags } from '@nestjs/swagger';
import { InternalServerErrorResponseDto } from '../dto/response/internal-server-error.response.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Seat')
@ApiForbiddenResponse({ type: InternalServerErrorResponseDto })
@ApiBasicAuth()
@Controller({ path: 'seat', version: '1' })
export class SeatController {
  private readonly logger = new Logger(SeatController.name);
  constructor(private readonly seatService: SeatService) { }

  @UseGuards(AuthGuard('basic'))
  @Post()
  async create(@Body() createSeatDto: CreateSeatDto) {
    this.logger.log('[POST] /api/v1/seat');
    try {
      const result = await this.seatService.create(createSeatDto);
      this.logger.log(
        `Return result of create seat data ${JSON.stringify(createSeatDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get()
  async findAll() {
    this.logger.log('[GET] /api/v1/seat');
    try {
      const seats = await this.seatService.findAll();
      this.logger.log(
        `Return all data seat with id`,
      );
      return seats;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get(':id')
  async findOne(@Param('id') id: string) {
    this.logger.log('[GET] /api/v1/seat');
    try {
      const seats = await this.seatService.findOne(id);
      this.logger.log(
        `Return data seat with id ${JSON.stringify(id)}`,
      );
      return seats;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateSeatDto: UpdateSeatDto) {
    this.logger.log('[PATCH] /api/v1/seat');
    try {
      const result = await this.seatService.update(id,updateSeatDto);
      this.logger.log(
        `Return result of update seat data ${JSON.stringify(updateSeatDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Delete(':id')
  async remove(@Param('id') id: string) {
    this.logger.log('[DELETE] /api/v1/seat');
    try {
      const result = await this.seatService.remove(id);
      this.logger.log(
        `Return result of delete seat data with id ${id}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
}
