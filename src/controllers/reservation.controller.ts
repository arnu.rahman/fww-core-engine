import {
  Body,
  Controller,
  Get,
  HttpCode,
  Logger,
  Param,
  Post,
  ForbiddenException,
  UseGuards,
} from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';
import { AuthGuard } from '@nestjs/passport';
import { ApiBasicAuth, ApiInternalServerErrorResponse, ApiTags } from '@nestjs/swagger';
import { ReservationDto } from '../dto/core/reservation.dto';
import {
  IPaymentMaster,
  IPaymentUpdate,
} from '../interface/payment.interface';
import { IReservationUpdateRequest } from '../interface/reservation-update.interface';
import { ReservationService } from '../services/reservation.service';
import { EventPatternMessage } from '../helpers/enum/pattern-message.enum';
import { InternalServerErrorResponseDto } from '../dto/response/internal-server-error.response.dto';

@ApiTags('Reservation')
@ApiInternalServerErrorResponse({ type: InternalServerErrorResponseDto })
@ApiBasicAuth()
@Controller({ path: 'reservation', version: '1' })
export class ReservationController {
  private readonly logger = new Logger(ReservationController.name);
  constructor(private readonly reservationService: ReservationService) { }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post()
  async newReservation(@Body() reservationDto: ReservationDto) {
    this.logger.log(`[POST] /api/v1/reservation`);
    try {
      const reservation = await this.reservationService.createReservation(
        reservationDto,
      );
      this.logger.log('Return data reservation');
      return reservation;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('update')
  async updateReservation(@Body() data: any) {
    this.logger.log(`[POST] /api/v1/reservation/update`);
    try {
      const dataReservation = data as IReservationUpdateRequest;
      const reservation = await this.reservationService.updateReservation(
        dataReservation,
      );
      this.logger.log('Return data reservation');
      return reservation;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Get(':id')
  async getReservation(@Param('id') id: string) {
    this.logger.log(`[POST] /api/v1/reservation/${id}`);
    try {
      const reservation = await this.reservationService.getReservation(id);
      this.logger.log('Return data reservation');
      return reservation;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get('by-code/:identityNumber/:reservationCode')
  async getByReservationCode(
    @Param('identityNumber') identityNumber: string,
    @Param('reservationCode') reservationCode: string,
  ) {
    this.logger.log(
      `[GET] /api/v1/reservation/by-code/${identityNumber}/${reservationCode}`,
    );
    try {
      const reservation = await this.reservationService.getReservationByCode(
        identityNumber,
        reservationCode,
      );
      this.logger.log('Return data reservation');
      return reservation;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('charge-payment')
  async chargePayment(@Payload() data: any) {
    this.logger.log(`[POST] /api/v1/reservation/charge-payment`);
    try {
      const paymentData = data as IPaymentMaster;
      const paymentCharge = await this.reservationService.chargePayment(paymentData)
      this.logger.log('Charge payment done');
      return paymentCharge;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @EventPattern(EventPatternMessage.UPDATE_RESERVATION)
  async handleUpdateReservationSystemTask(@Payload() data: any) {
    this.logger.log(`[EP] ${EventPatternMessage.UPDATE_RESERVATION}`);
    try {
      const dataReservation = data as IReservationUpdateRequest;
      await this.reservationService.updateReservation(dataReservation);
      this.logger.log('Update reservation done');
    } catch (error) {
      throw new ForbiddenException(
        'Error in handleUpdateReservation',
      );
    }
  }

  @EventPattern(EventPatternMessage.CHARGE_PAYMENT)
  async handleChargePayment(@Payload() data: any) {
    this.logger.log(`[EP] ${EventPatternMessage.CHARGE_PAYMENT}`);
    try {
      const paymentData = data as IPaymentMaster;
      await this.reservationService.chargePayment(paymentData);
      this.logger.log('Charge payment done');
    } catch (error) {
      throw new ForbiddenException('Error in handleChargePayment');
    }
  }

  @EventPattern(EventPatternMessage.UPDATE_PAYMENT)
  async handleUpdatePayment(@Payload() data: any) {
    this.logger.log(`[EP] ${EventPatternMessage.UPDATE_PAYMENT}`);
    try {
      const paymentData = data as IPaymentUpdate;
      await this.reservationService.updatePayment(paymentData);
      this.logger.log('Update payment done');
    } catch (error) {
      throw new ForbiddenException('Error in handleUpdatePayment');
    }
  }
}
