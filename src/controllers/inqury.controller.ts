import {
  Body,
  Controller,
  Get,
  HttpCode,
  Logger,
  Param,
  Post,
  Query,
  ForbiddenException,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBasicAuth,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { FilterAirportDto } from '../dto/core/filter-airport.dto';
import { FilterFlightDto } from '../dto/core/filter-flight.dto';
import { FlightCandidateDto } from '../dto/core/flight-candidate.dto';
import { FlightSeatDto } from '../dto/core/flight-seat.dto';
import { InternalServerErrorResponseDto } from '../dto/response/internal-server-error.response.dto';
import { ListAirportResponseDto } from '../dto/response/list-airport.response.dto';
import { ListFlightResponseDto } from '../dto/response/list-flight.response.dto';
import { SeatClassDto } from '../dto/core/seat-class.dto';
import { InquiryService } from '../services/inquiry.service';

@ApiTags('Inquiry')
@ApiInternalServerErrorResponse({ type: InternalServerErrorResponseDto })
@ApiBasicAuth()
@Controller({ path: 'inquiry', version: '1' })
export class InquiryController {
  private readonly logger = new Logger(InquiryController.name);
  constructor(private readonly inquiryService: InquiryService) {}

  @ApiOkResponse({ type: ListAirportResponseDto })
  @ApiQuery({ type: FilterAirportDto })
  @UseGuards(AuthGuard('basic'))
  @Get('airport')
  //get airport data by filter (icao, iata, name, city)
  async getAirport(@Query() query: FilterAirportDto) {
    this.logger.log('[GET] /api/v1/inquiry/airport');
    try {
      const airports = await this.inquiryService.searchAirport(query);
      this.logger.log(
        `Return data airports with criteria ${JSON.stringify(query)}`,
      );
      return airports;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @ApiOkResponse({ type: ListFlightResponseDto })
  @ApiQuery({ type: FilterFlightDto })
  @UseGuards(AuthGuard('basic'))
  @Get('flight')
  //get flight by filter origin dan destination
  async getFlight(@Query() query: FilterFlightDto) {
    this.logger.log('[GET] /api/v1/inquiry/flight');
    try {
      const flights = await this.inquiryService.filterFlight(query);
      this.logger.log(
        `Return data flights with criteria ${JSON.stringify(query)}`,
      );
      return flights;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Post('flight-candidate')
  @HttpCode(200)
  async getFlightCandidate(@Body() flightDto: FlightCandidateDto) {
    this.logger.log('[POST] /api/v1/inquiry/flight-candidate');
    try {
      const flights = await this.inquiryService.searchFlight(flightDto);
      this.logger.log(
        `Return data flight candidates ${JSON.stringify(flightDto)}`,
      );
      return flights;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Post('seat-validate')
  @HttpCode(200)
  async seatValidate(@Body() flightSeatDto: FlightSeatDto) {
    this.logger.log(`[POST] /api/v1/inquiry/seat-validate`);
    try {
      const validate = await this.inquiryService.seatValidate(flightSeatDto);
      this.logger.log(
        `Return validating result flight seat with with criteria ${JSON.stringify(
          flightSeatDto,
        )}`,
      );
      return validate;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get('flight/:id/seat')
  async getFlightSeatsById(
    @Param('id') id: string,
    @Query() query: SeatClassDto,
  ) {
    this.logger.log(`[GET] /api/v1/inquiry/flight/${id}/seat`);
    try {
      const flightSeats = await this.inquiryService.getFlightSeatsById(
        id,
        query,
      );
      this.logger.log(`Return data flight ${id} with seats`);
      return flightSeats;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get('flight/:id/baggage')
  async getFlightBaggagesById(@Param('id') id: string) {
    this.logger.log(`[GET] /api/v1/inquiry/flight/${id}/baggage`);
    try {
      const flightBaggages = await this.inquiryService.getFlightBaggagesById(
        id,
      );
      this.logger.log(`Return data flight ${id} with baggages`);
      return flightBaggages;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get('flight/:id/airport')
  async getFlightAirportsById(@Param('id') id: string) {
    this.logger.log(`[GET] /api/v1/inquiry/flight/${id}/airport`);
    try {
      const flightAirports = await this.inquiryService.getFlightAirportsById(
        id,
      );
      this.logger.log(`Return data flight ${id} with airports`);
      return flightAirports;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
}
