import { Controller, Logger, Get, Post, Body, Patch, Param, Delete, UseGuards, ForbiddenException } from '@nestjs/common';
import { BaggageService } from '../services/baggage.service';
import { CreateBaggageDto } from '../dto/request/create-baggage.dto';
import { UpdateBaggageDto } from '../dto/request/update-baggage.dto';
import { ApiBasicAuth, ApiForbiddenResponse, ApiTags } from '@nestjs/swagger';
import { InternalServerErrorResponseDto } from '../dto/response/internal-server-error.response.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Baggage')
@ApiForbiddenResponse({ type: InternalServerErrorResponseDto })
@ApiBasicAuth()
@Controller({ path: 'baggage', version: '1' })
export class BaggageController {
  private readonly logger = new Logger(BaggageController.name);
  constructor(private readonly baggageService: BaggageService) { }

  @UseGuards(AuthGuard('basic'))
  @Post()
  async create(@Body() createBaggageDto: CreateBaggageDto) {
    this.logger.log('[POST] /api/v1/baggage');
    try {
      const result = await this.baggageService.create(createBaggageDto);
      this.logger.log(
        `Return result of create baggage data ${JSON.stringify(createBaggageDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get()
  async findAll() {
    this.logger.log('[GET] /api/v1/baggage');
    try {
      const baggages = await this.baggageService.findAll();
      this.logger.log(
        `Return all data baggage with id`,
      );
      return baggages;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get(':id')
  async findOne(@Param('id') id: string) {
    this.logger.log('[GET] /api/v1/baggage');
    try {
      const baggages = await this.baggageService.findOne(id);
      this.logger.log(
        `Return data baggage with id ${JSON.stringify(id)}`,
      );
      return baggages;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateBaggageDto: UpdateBaggageDto) {
    this.logger.log('[PATCH] /api/v1/baggage');
    try {
      const result = await this.baggageService.update(id,updateBaggageDto);
      this.logger.log(
        `Return result of update baggage data ${JSON.stringify(updateBaggageDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Delete(':id')
  async remove(@Param('id') id: string) {
    this.logger.log('[DELETE] /api/v1/baggage');
    try {
      const result = await this.baggageService.remove(id);
      this.logger.log(
        `Return result of delete baggage data with id ${id}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
}
