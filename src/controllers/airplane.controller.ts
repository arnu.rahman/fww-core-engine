import { Controller, Logger, Get, Post, Body, Patch, Param, Delete, UseGuards, ForbiddenException } from '@nestjs/common';
import { AirplaneService } from '../services/airplane.service';
import { CreateAirplaneDto } from '../dto/request/create-airplane.dto';
import { UpdateAirplaneDto } from '../dto/request/update-airplane.dto';
import { ApiBasicAuth, ApiForbiddenResponse, ApiTags } from '@nestjs/swagger';
import { InternalServerErrorResponseDto } from '../dto/response/internal-server-error.response.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Airplane')
@ApiForbiddenResponse({ type: InternalServerErrorResponseDto })
@ApiBasicAuth()
@Controller({ path: 'airplane', version: '1' })
export class AirplaneController {
  private readonly logger = new Logger(AirplaneController.name);
  constructor(private readonly airplaneService: AirplaneService) { }

  @UseGuards(AuthGuard('basic'))
  @Post()
  async create(@Body() createAirplaneDto: CreateAirplaneDto) {
    this.logger.log('[POST] /api/v1/airplane');
    try {
      const result = await this.airplaneService.create(createAirplaneDto);
      this.logger.log(
        `Return result of create airplane data ${JSON.stringify(createAirplaneDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get()
  async findAll() {
    this.logger.log('[GET] /api/v1/airplane');
    try {
      const airplanes = await this.airplaneService.findAll();
      this.logger.log(
        `Return all data airplane with id`,
      );
      return airplanes;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get(':id')
  async findOne(@Param('id') id: string) {
    this.logger.log('[GET] /api/v1/airplane');
    try {
      const airplanes = await this.airplaneService.findOne(id);
      this.logger.log(
        `Return data airplane with id ${JSON.stringify(id)}`,
      );
      return airplanes;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateAirplaneDto: UpdateAirplaneDto) {
    this.logger.log('[PATCH] /api/v1/airplane');
    try {
      const result = await this.airplaneService.update(id,updateAirplaneDto);
      this.logger.log(
        `Return result of update airplane data ${JSON.stringify(updateAirplaneDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Delete(':id')
  async remove(@Param('id') id: string) {
    this.logger.log('[DELETE] /api/v1/airplane');
    try {
      const result = await this.airplaneService.remove(id);
      this.logger.log(
        `Return result of delete airplane data with id ${id}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
}
