import { Controller, Logger, Get, Post, Body, Patch, Param, Delete, UseGuards, ForbiddenException } from '@nestjs/common';
import { FlightService } from '../services/flight.service';
import { CreateFlightDto } from '../dto/request/create-flight.dto';
import { UpdateFlightDto } from '../dto/request/update-flight.dto';
import { ApiBasicAuth, ApiForbiddenResponse, ApiTags } from '@nestjs/swagger';
import { InternalServerErrorResponseDto } from '../dto/response/internal-server-error.response.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Flight')
@ApiForbiddenResponse({ type: InternalServerErrorResponseDto })
@ApiBasicAuth()
@Controller({ path: 'flight', version: '1' })
export class FlightController {
  private readonly logger = new Logger(FlightController.name);
  constructor(private readonly flightService: FlightService) { }

  @UseGuards(AuthGuard('basic'))
  @Post()
  async create(@Body() createFlightDto: CreateFlightDto) {
    this.logger.log('[POST] /api/v1/flight');
    try {
      const result = await this.flightService.create(createFlightDto);
      this.logger.log(
        `Return result of create flight data ${JSON.stringify(createFlightDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get()
  async findAll() {
    this.logger.log('[GET] /api/v1/flight');
    try {
      const flights = await this.flightService.findAll();
      this.logger.log(
        `Return all data flight with id`,
      );
      return flights;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Get(':id')
  async findOne(@Param('id') id: string) {
    this.logger.log('[GET] /api/v1/flight');
    try {
      const flights = await this.flightService.findOne(id);
      this.logger.log(
        `Return data flight with id ${JSON.stringify(id)}`,
      );
      return flights;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateFlightDto: UpdateFlightDto) {
    this.logger.log('[PATCH] /api/v1/flight');
    try {
      const result = await this.flightService.update(id,updateFlightDto);
      this.logger.log(
        `Return result of update flight data ${JSON.stringify(updateFlightDto)}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Delete(':id')
  async remove(@Param('id') id: string) {
    this.logger.log('[DELETE] /api/v1/flight');
    try {
      const result = await this.flightService.remove(id);
      this.logger.log(
        `Return result of delete flight data with id ${id}`,
      );
      return result;
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
}
