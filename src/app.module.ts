import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';
import { dataSourceOptions } from './config/datasource.config';
import { AuthModule } from './auth/auth.module';
import { InquiryController } from './controllers/inqury.controller';
import { ReservationController } from './controllers/reservation.controller';
import { InquiryService } from './services/inquiry.service';
import { ReservationService } from './services/reservation.service';
import { AirportRepository } from './repository/airport.repository';
import { FlightRepository } from './repository/flight.repository';
import { PassengerRepository } from './repository/passenger.repository';
import { ReservationRepository } from './repository/reservation.repository';
import { ReservationJourneyRepository } from './repository/reservation-journey.repository';
import { PaymentRepository } from './repository/payment.repository';
import { PriceRepository } from './repository/price.repository';
import { SeatRepository } from './repository/seat.repository';
import { AirportController } from './controllers/airport.controller';
import { AirportService } from './services/airport.service';
import { AirplaneController } from './controllers/airplane.controller';
import { BaggageController } from './controllers/baggage.controller';
import { FlightController } from './controllers/flight.controller';
import { PriceController } from './controllers/price.controller';
import { SeatController } from './controllers/seat.controller';
import { AirplaneService } from './services/airplane.service';
import { BaggageService } from './services/baggage.service';
import { FlightService } from './services/flight.service';
import { PriceService } from './services/price.service';
import { SeatService } from './services/seat.service';
import { AirplaneRepository } from './repository/airplane.repository';
import { BaggageRepository } from './repository/baggage.repository';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot(dataSourceOptions),
    LoggerModule.forRoot({
      pinoHttp: {
        formatters: {
          level: (label) => {
            return { level: label.toUpperCase() };
          },
        },
        customLevels: {
          emerg: 80,
          alert: 70,
          crit: 60,
          error: 50,
          warn: 40,
          notice: 30,
          info: 20,
          debug: 10,
        },
        useOnlyCustomLevels: true,
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
            colorize: true,
            levelFirst: true,
            translateTime: 'SYS:standard',
            ignore: 'hostname,pid',
          }
        }
      }
    }),
    AuthModule,
  ],
  controllers: [
    AirportController,
    AirplaneController,
    BaggageController,
    FlightController,
    PriceController,
    SeatController,
    InquiryController,
    ReservationController
  ],
  providers: [
    AirportService,
    AirplaneService,
    BaggageService,
    FlightService,
    PriceService,
    SeatService,
    InquiryService,
    ReservationService,
    AirplaneRepository,
    AirportRepository,
    BaggageRepository,
    FlightRepository,
    PassengerRepository,
    ReservationRepository,
    ReservationJourneyRepository,
    PaymentRepository,
    PriceRepository,
    SeatRepository,
  ],
})
export class AppModule { }
