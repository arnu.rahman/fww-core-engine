import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { CreateFlightDto } from '../dto/request/create-flight.dto';
import { UpdateFlightDto } from '../dto/request/update-flight.dto';
import { FlightRepository } from '../repository/flight.repository';
import * as moment from 'moment';
import { AirplaneRepository } from '../repository/airplane.repository';
import { AirportRepository } from '../repository/airport.repository';
import { InterfaceService } from './interface.service';

@Injectable()
export class FlightService extends InterfaceService {
  private readonly logger = new Logger(FlightService.name)
  constructor(
    private readonly flightRepo: FlightRepository,
    private readonly airplaneRepo: AirplaneRepository,
    private readonly airportRepo: AirportRepository,
  ) { super() }

  async create(createDto: CreateFlightDto) {
    //check if airplane exist
    await this.isAirplaneExist(createDto.airplane.id);

    //check if origin exist
    await this.isAirportExist(createDto.origin.id);

    //check if destination exist
    await this.isAirportExist(createDto.destination.id);

    this.logger.log('Submit data of flight');
    return this.flightRepo.save(createDto);
  }

  async update(id: string, updateDto: UpdateFlightDto) {
    //check if data exist
    await this.isExist(id);

    //check if airplane exist
    await this.isAirplaneExist(updateDto.airplane.id);

    //check if origin exist
    await this.isAirportExist(updateDto.origin.id);

    //check if destination exist
    await this.isAirportExist(updateDto.destination.id);

    this.logger.log('Update data of flight');
    await this.flightRepo.update(id, updateDto)
    return updateDto;
  }

  async remove(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);
    checkData.deletedAt = moment().format("YYYY-MM-DD HH:mm:ss")

    this.logger.log('Delete data of flight');
    await this.flightRepo.update(id, checkData);
    return {
      id: checkData.id,
      deletedAt: checkData.deletedAt
    }
  }

  async isExist(id: string) {
    //check if data exist
    const flightData = await this.flightRepo.findOne({
      where: {
        id: id
      },
      relations: {
        airplane: true,
        origin: true,
        destination: true,
        baggage: true,
        prices: true,
      },
    });

    if (!flightData) {
      throw new NotFoundException('Flight not found');
    }
    const originUtc = this.getTimezone(flightData.origin.timezone);
    const depature = this.getTimezone(flightData.destination.timezone);
    const defaultDate = moment().format("YYYY-MM-DD");
    flightData.departureTime = moment(`${defaultDate} ${flightData.departureTime}`).add(originUtc, 'hours').format('HH:mm:ss')
    flightData.arrivalTime = moment(`${defaultDate} ${flightData.arrivalTime}`).add(depature, 'hours').format('HH:mm:ss')
    flightData.boardingTime = moment(`${defaultDate} ${flightData.boardingTime}`).add(originUtc, 'hours').format('HH:mm:ss')

    return flightData
  }

  async isAirplaneExist(airplaneId: string) {
    //check if data exist
    const airplaneData = await this.airplaneRepo.findOne({
      where: {
        id: airplaneId
      }
    });

    if (!airplaneData) {
      throw new NotFoundException('Airplane not found');
    }

    return airplaneData
  }

  async isAirportExist(airportId: string) {
    //check if data exist
    const airportData = await this.airportRepo.findOne({
      where: {
        id: airportId
      }
    });

    if (!airportData) {
      throw new NotFoundException('Airport not found');
    }

    return airportData
  }

  async findAll() {
    this.logger.log('get all data of flight');
    return this.flightRepo.find({
      relations: {
        airplane: true,
        origin: true,
        destination: true,
        baggage: true,
        prices: true,
      },
    }).then(flight => {
      let results = flight.map(val => {
        const originUtc = this.getTimezone(val.origin.timezone);
        const depature = this.getTimezone(val.destination.timezone);
        const defaultDate = moment().format("YYYY-MM-DD");
        val.departureTime = moment(`${defaultDate} ${val.departureTime}`).add(originUtc, 'hours').format('HH:mm:ss')
        val.arrivalTime = moment(`${defaultDate} ${val.arrivalTime}`).add(depature, 'hours').format('HH:mm:ss')
        val.boardingTime = moment(`${defaultDate} ${val.boardingTime}`).add(originUtc, 'hours').format('HH:mm:ss')
        return val;
      });
      return results;
    }).catch(e => {
      throw new NotFoundException('flight data not found');
    });
  }

  async findOne(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);

    this.logger.log('get one data of flight');
    return checkData;
  }
}
