import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { CreatePriceDto } from '../dto/request/create-price.dto';
import { UpdatePriceDto } from '../dto/request/update-price.dto';
import { PriceRepository } from '../repository/price.repository';
import * as moment from 'moment';
import { FlightRepository } from '../repository/flight.repository';

@Injectable()
export class PriceService {
  private readonly logger = new Logger(PriceService.name)
  constructor(
    private readonly priceRepo: PriceRepository,
    private readonly flightRepo: FlightRepository,
  ) { }

  async create(createDto: CreatePriceDto) {
    //check if flight exist
    await this.isFlightExist(createDto.flight.id);

    this.logger.log('Submit data of price');
    return this.priceRepo.save(createDto);
  }

  async update(id: string, updateDto: UpdatePriceDto) {
    //check if data exist
    await this.isExist(id);

    //check if flight exist
    await this.isFlightExist(updateDto.flight.id);

    this.logger.log('Update data of price');
    await this.priceRepo.update(id, updateDto)
    return updateDto;
  }

  async remove(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);
    checkData.deletedAt = moment().format("YYYY-MM-DD HH:mm:ss")

    this.logger.log('Delete data of price');
    await this.priceRepo.update(id, checkData);
    return {
      id: checkData.id,
      deletedAt: checkData.deletedAt
    }

  }

  async isExist(id: string) {
    //check if data exist
    const priceData = await this.priceRepo.findOne({
      where: {
        id: id
      },
      relations: {
        flight: { airplane: true, origin: true, destination: true },
      },
    });

    if (!priceData) {
      throw new NotFoundException('Price not found');
    }

    return priceData
  }

  async isFlightExist(flightId: string) {
    //check if data exist
    const flightData = await this.flightRepo.findOne({
      where: {
        id: flightId
      }
    });

    if (!flightData) {
      throw new NotFoundException('Flight not found');
    }

    return flightData
  }

  async findAll() {
    this.logger.log('get all data of price');
    return this.priceRepo.find({
      relations: {
        flight: { airplane: true, origin: true, destination: true },
      },
    });
  }

  async findOne(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);

    this.logger.log('get one data of price');
    return checkData;
  }
}
