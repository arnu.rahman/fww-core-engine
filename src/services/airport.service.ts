import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { CreateAirportDto } from '../dto/request/create-airport.dto';
import { UpdateAirportDto } from '../dto/request/update-airport.dto';
import { AirportRepository } from '../repository/airport.repository';
import * as moment from 'moment';

@Injectable()
export class AirportService {
  private readonly logger = new Logger(AirportService.name)
  constructor(
    private readonly airportRepo: AirportRepository,
  ) { }

  async create(createDto: CreateAirportDto) {
    this.logger.log('Submit data of airport');

    return this.airportRepo.save(createDto);
  }

  async update(id: string, updateDto: UpdateAirportDto) {
    //check if data exist
    await this.isExist(id);

    this.logger.log('Update data of airport');
    await this.airportRepo.update(id, updateDto)
    return updateDto;
  }

  async remove(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);
    checkData.deletedAt = moment().format("YYYY-MM-DD HH:mm:ss")

    this.logger.log('Delete data of airport');
    await this.airportRepo.update(id, checkData);
    return {
      id: checkData.id,
      deletedAt: checkData.deletedAt
    }
  }

  async isExist(id: string) {
    //check if data exist
    const airportData = await this.airportRepo.findOne({
      where: {
        id: id
      }
    });

    if (!airportData) {
      throw new NotFoundException('Airport not found');
    }

    return airportData
  }

  async findAll() {
    this.logger.log('get all data of airport');
    return this.airportRepo.find();
  }

  async findOne(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);

    this.logger.log('get one data of airport');
    return checkData;
  }
}
