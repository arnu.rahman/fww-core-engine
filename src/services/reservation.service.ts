import {
  Injectable,
  Logger,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ReservationDto } from '../dto/core/reservation.dto';
import {
  IReservationNew,
  IReservationJourney,
} from '../interface/reservation-new.interface';
import { IPassenger } from '../interface/passenger.interface';
import { PassengerRepository } from '../repository/passenger.repository';

import { ReservationRepository } from '../repository/reservation.repository';
import {
  IReservationUpdateData,
  IReservationUpdateRequest,
} from '../interface/reservation-update.interface';
import { ReservationJourneyRepository } from '../repository/reservation-journey.repository';
import {
  IPaymentMaster,
  IPaymentUpdate,
} from '../interface/payment.interface';
import { PaymentRepository } from '../repository/payment.repository';
import { Reservation } from '../entities/reservation.entity';
import { EReservationStatus } from '../helpers/enum/reservation-status.enum';
import { Payment } from '../entities/payment.entity';

@Injectable()
export class ReservationService {
  private readonly logger = new Logger(ReservationService.name);
  constructor(
    private readonly reservationRepo: ReservationRepository,
    private readonly reservationJourneyRepo: ReservationJourneyRepository,
    private readonly passengerRepo: PassengerRepository,
    private readonly paymentRepo: PaymentRepository,
  ) { }

  async createReservation(
    reservationDto: ReservationDto,
  ): Promise<Reservation> {
    const dataPassenger: IPassenger = {
      identityCategory: reservationDto.identityCategory,
      identityNumber: reservationDto.identityNumber,
      name: reservationDto.name,
      phone: reservationDto.phone,
      email: reservationDto.email,
      birthDate: reservationDto.birthDate,
    };

    const dataJourney: IReservationJourney = {
      description: EReservationStatus.NEW,
      journeyTime: reservationDto.reservationTime,
    };

    const dataReservation: IReservationNew = {
      partnerId: reservationDto.partnerId,
      memberId: reservationDto.memberId,
      passenger: null,
      flightDate: reservationDto.flightDate,
      flight: { id: reservationDto.flightId },
      seat: { id: reservationDto.seatId },
      priceActual: reservationDto.priceActual,
      currentStatus: EReservationStatus.NEW,
      journeys: [dataJourney],
    };

    const passenger = await this.passengerRepo.savePassenger(dataPassenger);
    dataReservation.passenger = passenger
    const newReservation = await this.reservationRepo.saveReservation(
      dataReservation,
    );

    if (passenger && newReservation) {
      this.logger.log('Process new reservation');
      dataJourney.reservation ={ id: newReservation.id }
      await this.reservationJourneyRepo.write(dataJourney);
      return newReservation;
    } else {
      throw new UnprocessableEntityException('Failed to save reservation');
    }
  }

  async updateReservation(
    reservationUpdateRequest: IReservationUpdateRequest,
  ): Promise<Reservation> {
    const reservationUpdateData: IReservationUpdateData = {
      currentStatus: reservationUpdateRequest.status,
      bookingCode: reservationUpdateRequest.bookingCode,
      reservationCode: reservationUpdateRequest.reservationCode,
    };

    const reservationJourneyData: IReservationJourney = {
      reservation: { id: reservationUpdateRequest.id },
      description: reservationUpdateRequest.status,
      journeyTime: reservationUpdateRequest.journeyTime,
    };

    const result = await Promise.all([
      await this.reservationRepo.updateReservation(
        reservationUpdateRequest.id,
        reservationUpdateData,
      ),
      await this.reservationJourneyRepo.write(reservationJourneyData),
    ]);

    const reservation = result[0];

    this.logger.log('Process update reservation');
    return reservation;
  }

  async getReservation(id: string): Promise<Reservation> {
    const reservation = await this.reservationRepo.findReservationDetailById(
      id,
    );

    if (!reservation) {
      throw new NotFoundException('Reservation not found');
    }

    this.logger.log('Process data reservation');
    return reservation;
  }

  async getReservationByCode(
    identityNumber: string,
    reservationCode: string,
  ): Promise<Reservation> {
    const reservation = await this.reservationRepo.findReservationByCode(
      identityNumber,
      reservationCode,
    );

    if (!reservation) {
      throw new NotFoundException('Reservation not found');
    }

    this.logger.log('Process data reservation');
    return reservation;
  }

  async chargePayment(dataPayment: IPaymentMaster): Promise<Payment> {
    const newPayment = await this.paymentRepo.writeNew(dataPayment);
    if (!newPayment) {
      throw new UnprocessableEntityException('Failed write payment');
    }

    this.logger.log('Process charge payment');
    return newPayment;
  }

  async updatePayment(dataPayment: IPaymentUpdate): Promise<Payment> {
    const payment = await this.paymentRepo.updateStatus(dataPayment);
    this.logger.log('Process update payment');
    return payment;
  }
}
