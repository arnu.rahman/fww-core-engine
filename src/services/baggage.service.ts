import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { CreateBaggageDto } from '../dto/request/create-baggage.dto';
import { UpdateBaggageDto } from '../dto/request/update-baggage.dto';
import { BaggageRepository } from '../repository/baggage.repository';
import * as moment from 'moment';
import { FlightRepository } from '../repository/flight.repository';

@Injectable()
export class BaggageService {
  private readonly logger = new Logger(BaggageService.name)
  constructor(
    private readonly baggageRepo: BaggageRepository,
    private readonly flightRepo: FlightRepository,
  ) { }

  async create(createDto: CreateBaggageDto) {
    //check if flight exist
    await this.isFlightExist(createDto.flight.id);

    this.logger.log('Submit data of baggage');
    return this.baggageRepo.save(createDto);
  }

  async update(id: string, updateDto: UpdateBaggageDto) {
    //check if data exist
    await this.isExist(id);

    //check if flight exist
    await this.isFlightExist(updateDto.flight.id);

    this.logger.log('Update data of baggage');
    await this.baggageRepo.update(id, updateDto)
    return updateDto;
  }

  async remove(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);
    checkData.deletedAt = moment().format("YYYY-MM-DD HH:mm:ss")

    this.logger.log('Delete data of baggage');
    await this.baggageRepo.update(id, checkData);
    return {
      id: checkData.id,
      deletedAt: checkData.deletedAt
    }
  }

  async isExist(id: string) {
    //check if data exist
    const baggageData = await this.baggageRepo.findOne({
      where: {
        id: id
      },
      relations: {
        flight: { airplane: true, origin: true, destination: true },
      },
    });

    if (!baggageData) {
      throw new NotFoundException('Baggage not found');
    }

    return baggageData
  }

  async isFlightExist(flightId: string) {
    //check if data exist
    const flightData = await this.flightRepo.findOne({
      where: {
        id: flightId
      }
    });

    if (!flightData) {
      throw new NotFoundException('Flight not found');
    }

    return flightData
  }

  async findAll() {
    this.logger.log('get all data of baggage');
    return this.baggageRepo.find({
      relations: {
        flight: { airplane: true, origin: true, destination: true },
      },
    });
  }

  async findOne(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);

    this.logger.log('get one data of baggage');
    return checkData;
  }
}
