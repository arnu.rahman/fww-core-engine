import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { CreateAirplaneDto } from '../dto/request/create-airplane.dto';
import { UpdateAirplaneDto } from '../dto/request/update-airplane.dto';
import { AirplaneRepository } from '../repository/airplane.repository';
import * as moment from 'moment';

@Injectable()
export class AirplaneService {
  private readonly logger = new Logger(AirplaneService.name)
  constructor(
    private readonly airplaneRepo: AirplaneRepository,
  ) { }

  async create(createDto: CreateAirplaneDto) {
    this.logger.log('Submit data of airplane');
    return this.airplaneRepo.save(createDto);
  }

  async update(id: string, updateDto: UpdateAirplaneDto) {
    //check if data exist
    await this.isExist(id);

    this.logger.log('Update data of airplane');
    await this.airplaneRepo.update(id, updateDto)
    return updateDto;
  }

  async remove(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);
    checkData.deletedAt = moment().format("YYYY-MM-DD HH:mm:ss")

    this.logger.log('Delete data of airplane');
    await this.airplaneRepo.update(id, checkData);
    return {
      id: checkData.id,
      deletedAt: checkData.deletedAt
    }
  }

  async isExist(id: string) {
    //check if data exist
    const airplaneData = await this.airplaneRepo.findOne({
      where: {
        id: id
      }
    });

    if (!airplaneData) {
      throw new NotFoundException('Airplane not found');
    }

    return airplaneData
  }

  async findAll() {
    this.logger.log('get all data of airplane');
    return this.airplaneRepo.find();
  }

  async findOne(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);

    this.logger.log('get one data of airplane');
    return checkData;
  }
}
