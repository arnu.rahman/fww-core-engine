import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { CreateSeatDto } from '../dto/request/create-seat.dto';
import { UpdateSeatDto } from '../dto/request/update-seat.dto';
import { SeatRepository } from '../repository/seat.repository';
import * as moment from 'moment';
import { AirplaneRepository } from '../repository/airplane.repository';

@Injectable()
export class SeatService {
  private readonly logger = new Logger(SeatService.name)
  constructor(
    private readonly seatRepo: SeatRepository,
    private readonly airplaneRepo: AirplaneRepository,
  ) { }

  async create(createDto: CreateSeatDto) {
    //check if airplane exist
    await this.isAirplaneExist(createDto.airplane.id);

    this.logger.log('Submit data of seat');
    return this.seatRepo.save(createDto);
  }

  async update(id: string, updateDto: UpdateSeatDto) {
    //check if data exist
    await this.isExist(id);

    //check if airplane exist
    await this.isAirplaneExist(updateDto.airplane.id);

    this.logger.log('Update data of seat');
    await this.seatRepo.update(id, updateDto)
    return updateDto;
  }

  async remove(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);
    checkData.deletedAt = moment().format("YYYY-MM-DD HH:mm:ss")

    this.logger.log('Delete data of seat');
    await this.seatRepo.update(id, checkData);
    return {
      id: checkData.id,
      deletedAt: checkData.deletedAt
    }
  }

  async isExist(id: string) {
    //check if data exist
    const seatData = await this.seatRepo.findOne({
      where: {
        id: id
      },
      relations: {
        airplane: true,
      },
    });

    if (!seatData) {
      throw new NotFoundException('Seat not found');
    }

    return seatData
  }

  async isAirplaneExist(airplaneId: string) {
    //check if data exist
    const airplaneData = await this.airplaneRepo.findOne({
      where: {
        id: airplaneId
      }
    });

    if (!airplaneData) {
      throw new NotFoundException('Airplane not found');
    }

    return airplaneData
  }

  async findAll() {
    this.logger.log('get all data of seat');
    return this.seatRepo.find({
      relations: {
        airplane: true,
      },
    });
  }

  async findOne(id: string) {
    //check if data exist
    const checkData = await this.isExist(id);

    this.logger.log('get one data of seat');
    return checkData;
  }
}
