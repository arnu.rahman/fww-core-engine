import { Injectable } from '@nestjs/common';
import { ETimezone } from '../helpers/enum/timezone.enum';

@Injectable()
export class InterfaceService {
  getTimezone(airportTz: string) {
    let utcTime

    if(airportTz == ETimezone.WIB){
      utcTime = 0
    }else if(airportTz == ETimezone.WITA){
      utcTime = 1
    }else{
      utcTime = 2
    }

    return utcTime
  }
}
