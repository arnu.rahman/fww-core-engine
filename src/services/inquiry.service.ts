import {
  Injectable,
  Logger,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

import { FilterAirportDto } from '../dto/core/filter-airport.dto';
import { FilterFlightDto } from '../dto/core/filter-flight.dto';

import { AirportRepository } from '../repository/airport.repository';
import { FlightRepository } from '../repository/flight.repository';

import { Airport } from '../entities/airport.entity';
import { Flight } from '../entities/flight.entity';
import { FlightCandidateDto } from '../dto/core/flight-candidate.dto';
import { SeatClassDto } from '../dto/core/seat-class.dto';
import { ISeatValidate } from '../interface/seat-validate.interface';
import { PriceRepository } from '../repository/price.repository';
import { IPriceSearch } from '../interface/price-search.interface';
import { SeatRepository } from '../repository/seat.repository';
import { FlightSeatDto } from '../dto/core/flight-seat.dto';
import { ISeat } from '../interface/seat.interface';
import { IAirplaneWithSpecificSeat } from '../interface/airplane-with-specific-seat.interface';
import { IFlightWithSpecificSeat } from '../interface/flight-with-specific-seat.interface';
import * as moment from 'moment';
import { InterfaceService } from './interface.service';

@Injectable()
export class InquiryService extends InterfaceService {
  private readonly logger = new Logger(InquiryService.name);
  constructor(
    private readonly airportRepo: AirportRepository,
    private readonly flightRepo: FlightRepository,
    private readonly priceRepo: PriceRepository,
    private readonly seatRepo: SeatRepository,
  ) {super()}

  async searchAirport(query: FilterAirportDto): Promise<Airport[]> {
    const airports = await this.airportRepo.findWithQuery(query);

    if (airports.length === 0) {
      throw new NotFoundException('Airport not found');
    }

    this.logger.log('Process data airports');
    return airports;
  }

  async filterFlight(query: FilterFlightDto): Promise<Flight[]> {
    const flights = await this.flightRepo.findWithQuery(query);

    if (flights.length === 0) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flights');
    return flights.map(val => {
      const originUtc = this.getTimezone(val.origin.timezone);
      const depature = this.getTimezone(val.destination.timezone);
      const defaultDate = moment().format("YYYY-MM-DD");
      val.departureTime = moment(`${defaultDate} ${val.departureTime}`).add(originUtc, 'hours').format('HH:mm:ss')
      val.arrivalTime = moment(`${defaultDate} ${val.arrivalTime}`).add(depature, 'hours').format('HH:mm:ss')
      val.boardingTime = moment(`${defaultDate} ${val.boardingTime}`).add(originUtc, 'hours').format('HH:mm:ss')
      return val;
    });
  }

  async searchFlight(flightDto: FlightCandidateDto): Promise<Flight[]> {
    const flights = await this.flightRepo.findCandidate(flightDto);
    if (flights.length === 0) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flight candidates');
    return flights;
  }

  async seatValidate(flightSeatDto: FlightSeatDto): Promise<ISeatValidate> {
    const seat = await this.seatRepo.findOneBySeatId(flightSeatDto.seat);
    if (!seat) {
      throw new NotFoundException('Seat data not found');
    }

    const priceCriteria: IPriceSearch = {
      flight: flightSeatDto.flight,
      seatClass: seat.seatClass,
    };

    const resultFlightAndPrice = await Promise.all([
      this.flightRepo.findByIdIncludeSpecificSeat(flightSeatDto),
      this.priceRepo.findByFlightAndSeatClass(priceCriteria),
    ]);

    const flight = resultFlightAndPrice[0];
    const price = resultFlightAndPrice[1];

    if (!flight) {
      throw new UnprocessableEntityException('Invalid input seat info');
    }

    const specificSeat: ISeat = {
      id: flight.airplane.seats[0].id,
      code: flight.airplane.seats[0].code,
      seatClass: flight.airplane.seats[0].seatClass,
      side: flight.airplane.seats[0].seatSide,
      position: flight.airplane.seats[0].seatPosition,
    };

    const airplane: IAirplaneWithSpecificSeat = {
      id: flight.airplane.id,
      name: flight.airplane.name,
      registrationNumber: flight.airplane.registrationNumber,
      maxBusiness: flight.airplane.maxBusiness,
      maxEconomy: flight.airplane.maxEconomy,
      totalSeat: flight.airplane.totalSeat,
      seat: specificSeat,
    };

    const flightWithSpecificSeat: IFlightWithSpecificSeat = {
      id: flight.id,
      code: flight.code,
      origin: flight.origin,
      destination: flight.destination,
      airplane: airplane,
      departureTimeInWIB: flight.departureTime,
      arrivalTimeInWIB: flight.arrivalTime,
      durationInMinutes: flight.durationInMinutes,
    };

    this.logger.log('Process data flight include specific seat');
    return {
      isValid: true,
      flight: flightSeatDto.flight,
      origin: flightWithSpecificSeat.origin,
      destination: flightWithSpecificSeat.destination,
      airplane: flightWithSpecificSeat.airplane.id,
      seat: flightSeatDto.seat,
      seatClass: seat.seatClass,
      seatNumber: seat.code,
      price: price.price,
    } as ISeatValidate;
  }

  async getFlightBaggagesById(id: string): Promise<Flight> {
    const flight = await this.flightRepo.findByIdIncludeBaggages(id);

    if (!flight) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flight include baggages');
    return await this.interfaceFlightOne(flight)
  }

  async getFlightSeatsById(id: string, query: SeatClassDto): Promise<Flight> {
    const flight = await this.flightRepo.findByIdIncludeSeats(id, query);

    if (!flight) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flight include seats');
    return await this.interfaceFlightOne(flight)
  }

  async getFlightAirportsById(id: string): Promise<Flight> {
    const flight = await this.flightRepo.findByIdIncludeAirports(id);

    if (!flight) {
      throw new NotFoundException('Flight not found');
    }

    this.logger.log('Process data flight include airports');
    return await this.interfaceFlightOne(flight)
  }

  async interfaceFlightOne(flight: any){
    const originUtc = this.getTimezone(flight.origin.timezone);
    const depature = this.getTimezone(flight.destination.timezone);
    const defaultDate = moment().format("YYYY-MM-DD");
    flight.departureTime = moment(`${defaultDate} ${flight.departureTime}`).add(originUtc, 'hours').format('HH:mm:ss')
    flight.arrivalTime = moment(`${defaultDate} ${flight.arrivalTime}`).add(depature, 'hours').format('HH:mm:ss')
    flight.boardingTime = moment(`${defaultDate} ${flight.boardingTime}`).add(originUtc, 'hours').format('HH:mm:ss')

    return flight
  }
}
