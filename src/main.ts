import { NestFactory, Reflector } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Logger } from 'nestjs-pino';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import helmet from 'helmet';

import { AppModule } from './app.module';
import { ResponseInterceptor } from './helpers/utils/response.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.enableCors();
  app.use(helmet.hidePoweredBy());
  const moduleRef = app.select(AppModule);
  const reflector = moduleRef.get(Reflector);
  app.useGlobalInterceptors(new ResponseInterceptor(reflector));
  const logger = app.get(Logger);
  app.useLogger(logger);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidUnknownValues: true,
      transform: true,
      validateCustomDecorators: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );

  app.setGlobalPrefix('api');
  app.enableVersioning({
    type: VersioningType.URI,
  });

  const configSwagger = new DocumentBuilder()
    .setTitle('FWW Core Engine API Documentation')
    .setDescription('API Documentation for FWW Core Engine')
    .setVersion('1.0')
    .addBasicAuth()
    .setExternalDoc('Postman Collection', 'docs-json')
    .build();

  const configCustomSwagger: SwaggerCustomOptions = {
    customSiteTitle: 'FWW Core Engine',
    swaggerOptions: { docExpansion: 'none' },
  };

  const document = SwaggerModule.createDocument(app, configSwagger);
  SwaggerModule.setup('api/docs', app, document, configCustomSwagger);

  const configService = app.get(ConfigService);
  const serviceName = configService.get<string>('APP_NAME');
  const port = configService.get<number>('APP_PORT');
  await app.listen(port);
  logger.log(`${serviceName} is running on port ${port}`);

  app.connectMicroservice({
    transport: Transport.RMQ,
    options: {
      urls: [configService.get<string>('RABBIT_MQ_URI')],
      queue: configService.get<string>('RABBIT_MQ_CORE_QUEUE'),
      queueOptions: { durable: false },
      prefetchCount: 1,
    },
  });
  await app.startAllMicroservices();
  logger.log(
    `${serviceName} is listening ${configService.get<string>(
      'RABBIT_MQ_CORE_QUEUE',
    )}`,
  );
}

bootstrap()
  .then(null)
  .catch(() => console.log('error'));
